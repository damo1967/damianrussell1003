package Controllers
{
	import Helpers.*;
	
	import Models.*;
	
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	import flash.utils.setTimeout;
	
	import mx.effects.Fade;
	import mx.formatters.NumberFormatter;
	import mx.managers.CursorManager;

	public class InfoPagesController
	{
		private var _navigationController:NavigationController;
		private var _mainStage:DamianRussell;
		private var _imageLoader:Loader=new Loader();


		private var _fadeInImage:Fade=new Fade();
		private var _fadeOutImage:Fade=new Fade();

		private var _biographySource:String=new String("Content/Images/Biography.jpg");
		private var _contactsSource:String=new String("Content/Images/Contacts.jpg");
		private var _clientsSource:String=new String("Content/Images/Clients.jpg");
		
		private var _currentCategory:String;

		public function InfoPagesController(mainStage:Object, navigationController:Object)
		{
			_mainStage=mainStage as DamianRussell;
			_navigationController=navigationController as NavigationController;
		}

		public function InitialiseInfoPages():void
		{
			_mainStage.InfoPagesPanel.visible=true;
			_mainStage.InfoPagesPanel.includeInLayout=true;
			_mainStage.InfoPagesPanel.alpha=1;

			_mainStage.InfoPagesBackgroundImage.visible=true;
			
			//initialise fades;
				_fadeInImage.alphaTo=1;
				_fadeInImage.duration=1000;
				_fadeOutImage.alphaTo=0;
				_fadeOutImage.duration=1000;
		}

		public function LoadInfoPagesBackgroundImageStart(infoPageCategory:String):void
		{
			_currentCategory = infoPageCategory;
			
			var source:String;


			//open relevant panel
			switch (infoPageCategory)
			{
				case MenuItems.Biography:
					source=_biographySource;
					break;
				case MenuItems.Contacts:
					source=_contactsSource;
					break;
				case MenuItems.Clients:
					source=_clientsSource;
					break;
			}


			CursorManager.setBusyCursor();

			//create event listener to indicate when image finished downloading

			var urlRequest:URLRequest=new URLRequest(source);
			_imageLoader.load(urlRequest);
			_imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, LoadInfoPagesBackgroundImageEnd);
			_imageLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, LoadProgressListener);

			_mainStage.PercentLoaded.text="0%";
			_mainStage.Preloader.visible=true;


			/* _nextImage.addEventListener(HTTPStatusEvent.HTTP_STATUS, LoadImageEnd);
			 _nextImage.load(source); */

		}

		private function LoadProgressListener(e:ProgressEvent):void
		{
			var numberFormatter:NumberFormatter=new NumberFormatter();
			numberFormatter.precision=0;
			_mainStage.PercentLoaded.text=(numberFormatter.format(e.bytesLoaded / e.bytesTotal * 100)) + "%";
		}

		private function LoadInfoPagesBackgroundImageEnd(e:Event):void
		{
			//Next image finished downloading, fade out image swap with big image
			CursorManager.removeBusyCursor();
			_mainStage.Preloader.visible=false;
			_fadeInImage.end();
			_fadeOutImage.play([_mainStage.BigImage]);
			setTimeout(SwapImage, _fadeOutImage.duration);
		

			//update url to refelct currrent location
			_navigationController.updateURL(_currentCategory, "none", _currentCategory);
			
			FadeInText(_currentCategory);
			
			//set active button 
			_navigationController.ResetAllFooterNavigationButtons();
			switch (_currentCategory)
			{
				case MenuItems.Biography:
					_mainStage.BiographyButtonFooter.setStyle("color",_navigationController._textRollOverColor);
					_mainStage.BiographyButtonFooter.removeEventListener(MouseEvent.MOUSE_OUT,_navigationController.TextMouseOut);
					_mainStage.BiographyButtonFooter.removeEventListener(MouseEvent.MOUSE_OVER, _navigationController.TextMouseOver);
					break;
				case MenuItems.Contacts:
					_mainStage.ContactsButtonFooter.setStyle("color",_navigationController._textRollOverColor);
					_mainStage.ContactsButtonFooter.removeEventListener(MouseEvent.MOUSE_OUT,_navigationController.TextMouseOut);
					_mainStage.ContactsButtonFooter.removeEventListener(MouseEvent.MOUSE_OVER, _navigationController.TextMouseOver);
					break;
					
					
				case MenuItems.Clients:
					_mainStage.ClientsButtonFooter.setStyle("color",_navigationController._textRollOverColor);
					_mainStage.ClientsButtonFooter.removeEventListener(MouseEvent.MOUSE_OUT,_navigationController.TextMouseOut);
					_mainStage.ClientsButtonFooter.removeEventListener(MouseEvent.MOUSE_OVER, _navigationController.TextMouseOver);
					break;
			}
			

		
		}
		
			private function SwapImage():void
		{
			//swap next image with main image and fade in main image
			_mainStage.InfoPagesBackgroundImage.load(_imageLoader.content);
			_mainStage.InfoPagesBackgroundImage.width=_navigationController._imageWidth;
			_mainStage.InfoPagesBackgroundImage.height=_navigationController._imageHeight;
			//_navigationController.OnResizeElements();
			_mainStage.InfoPagesBackgroundImage.visible=true;
			_fadeInImage.play([_mainStage.InfoPagesBackgroundImage]);
		}

		private function FadeInText(currentCategory:String):void
		{
				switch (currentCategory)
			{
				case MenuItems.Biography:
					_fadeInImage.play([_mainStage.BiographyPage, _mainStage.BiographyPicture]);
					break;
				case MenuItems.Contacts:
				_mainStage.ContactHotSpot1.visible=true;
					_mainStage.ContactHotSpot2.visible=true;
					_fadeInImage.play([
					_mainStage.ContactPage, 
			    	_mainStage.ContactHotSpot1,
					_mainStage.ContactHotSpot2
				]);
				
					break;
				case MenuItems.Clients:
					_fadeInImage.play([_mainStage.ClientsPage]);
					break;
			}
		}

	}
}