package Controllers
{

	import Helpers.MenuItems;

	import Models.*;

	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	import flash.utils.setTimeout;

	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.controls.Image;
	import mx.effects.Fade;
	import mx.formatters.NumberFormatter;
	import mx.managers.CursorManager;


	public class GalleryController
	{

		private var _imagesPath:String=new String("CMSImages/");
		private var _imagesThumbnailsPath:String=new String("CMSImages/Thumbnails/");
		private var _mainStage:DamianRussell;
		private var _navigationController:NavigationController;

		private var _thumbnailWidth:int=100;
		private var _thumbnailHeight:int=66;
		public var _thumbnailGap:int=5;


		public var _currentThumbnailData:ThumbnailData;
		private var _imageLoader:Loader=new Loader();


		private var _fadeInImage:Fade=new Fade();
		private var _fadeOutImage:Fade=new Fade();

		public var _allThumbnailContainsers:ArrayCollection=new ArrayCollection();

		[Embed(source="../Content/Images/ThumbnailLoading.swf")]
		[Bindable]
		private var _thumbnailPreloadImageSource:Class;

		public var _totalThumbnails:Number;

		private var _currentCategory:String;
		private var _currentImageGuid:String;

		public function GalleryController(mainStage:Object, navigationController:Object)
		{
			_mainStage=mainStage as DamianRussell;
			_navigationController=navigationController as NavigationController;
		}

		public function InitialiseGallary(xMLData:XML, categoryToDisplay:String):void
		{

			_currentImageGuid=_navigationController._currentImageGuid;
			_currentCategory=categoryToDisplay;
//			//grab the application object
//			_mainStage=Application.application as DamianRussell;

			//prepare data
			var results:XMLList;
			if (categoryToDisplay != "Complete")
			{
				results=xMLData.pic.(category == categoryToDisplay);
			}
			else
			{
				results=xMLData.pic;
			}

			//add image thumbnails to middle menu
			var thumbnail:XML;
			var counter:int=0;
			var currentImageIndex:Number=0;

			//reset existing image and thumbnail settings
			_allThumbnailContainsers.removeAll();
			_mainStage.MiddleMenuThumbnailImages.removeAllChildren();
			_mainStage.BigImage.source=null;
			_navigationController._fadeInMenu.play([_mainStage.MiddleMenuShape]);

			if (results[0] != null)
			{
				for each (thumbnail in results)
				{
					//_mainStage.debug.text=_imagesThumbnailsPath + thumbnail.imageName;

					//count the number of items
					var thumbnailImage:Image=new Image();
					thumbnailImage.id=thumbnail.imageName;
					thumbnailImage.source=_imagesThumbnailsPath + thumbnail.imageName;
					thumbnailImage.addEventListener(MouseEvent.CLICK, LoadImageFromThumbnail);
					thumbnailImage.addEventListener(Event.COMPLETE, LoadThumbnailImageEnd);
					thumbnailImage.useHandCursor=true;
					thumbnailImage.buttonMode=true;
					thumbnailImage.toolTip=thumbnail.caption;
					thumbnailImage.setStyle("left", "1");
					thumbnailImage.setStyle("top", "1");


					var thumbnailPreloadImage:Image=new Image();
					thumbnailPreloadImage.source=_thumbnailPreloadImageSource;
					thumbnailPreloadImage.setStyle("left", "37");
					thumbnailPreloadImage.setStyle("top", "20");
					thumbnailPreloadImage.width=27;
					thumbnailPreloadImage.height=27;




					var thumbnailData:ThumbnailData=new ThumbnailData();
					thumbnailData.ImageName=thumbnail.imageName;
					thumbnailData.Caption=thumbnail.caption;
					thumbnailData.Index=counter;
					thumbnailData.Guid=thumbnail.guid;
					thumbnailImage.data=thumbnailData;

					//find imageToOpen
					if (thumbnailData.Guid == _currentImageGuid)
					{
						currentImageIndex=thumbnailData.Index;
					}

					var thumbnailContainer:Canvas=new Canvas;
					thumbnailContainer.width=102;
					thumbnailContainer.height=68;
					//thumbnailContainer.setStyle("backgroundColor", "#257496");

					thumbnailContainer.addChild(thumbnailImage);
					thumbnailContainer.addChild(thumbnailPreloadImage);

					//add thumbnail container to middle menu
					_mainStage.MiddleMenuThumbnailImages.addChild(thumbnailContainer);
					_mainStage.MiddleMenuThumbnailImages.width+=_thumbnailWidth + _thumbnailGap;
					_mainStage.MiddleMenuThumbnailImagesScroll.width+=_thumbnailWidth + _thumbnailGap;

					//add thumbnail container to arrayCollection
					_allThumbnailContainsers.addItem(thumbnailContainer);
					counter++;




				}

				//make footer menu function links visible
				_mainStage.Footer.visible=true;
				_mainStage.FooterMenuFunctionLinks.visible=true;

				//set the length of the thumbnails
				_navigationController._lengthOfThumbnailImages=(counter * _thumbnailWidth) + ((counter) * _thumbnailGap) + 123;
				_mainStage.MiddleMenuThumbnailImages.width=_navigationController._lengthOfThumbnailImages;

				//reset the thumbnail container to the left margin
				_mainStage.MiddleMenuThumbnailImagesScroll.x = 0;

				//make thumbnail container visible 
				_mainStage.MiddleMenu.alpha=1;

				//initialise fades;
				_fadeInImage.alphaTo=1;
				_fadeInImage.duration=1000;
				_fadeOutImage.alphaTo=0;
				_fadeOutImage.duration=1000;

				/* 	//load first image
				 var thumbnailData:ThumbnailData = LoadThumbnailData(0); */


				_totalThumbnails=counter;

				//make first thumbnail border white
				//var firstThumbnailContainer:Canvas=_mainStage.MiddleMenuThumbnailImages.getChildAt(0) as Canvas;
				//firstThumbnailContainer.setStyle("backgroundColor", "#ffffff");



				//load first image			
				LoadBigImageStart(currentImageIndex);



			}
			//display cateogory on middle menu
			if (categoryToDisplay == "Editorial") categoryToDisplay = "Interiors";
			_mainStage.Section.text=categoryToDisplay + " Gallery";
		}

		private function LoadThumbnailData(allThumbnailContainersIndex:Number):ThumbnailData
		{
			var thumbnailContainer:Canvas=_allThumbnailContainsers[allThumbnailContainersIndex];
			var thumbnail:Image=thumbnailContainer.getChildAt(0) as Image;
			return thumbnail.data as ThumbnailData;

		/*
		   thumbnailData.ImageName=results[resultsIndex].imageName;
		   thumbnailData.Caption=results[resultsIndex].caption;
		 thumbnailData.Index=resultsIndex; */

		}

		public function LoadImageFromThumbnail(e:MouseEvent):void
		{
			//grab big image name and caption from event target data
			//var fileName:String=new String();
			var thumbnailData:ThumbnailData=e.target.parent.data;

			/* 	fileName=e.target.parent.data.ImageName.toString();
			 */



			LoadBigImageStart(thumbnailData.Index);




		}

		private function LoadThumbnailImageEnd(e:Event):void
		{
			//make thumbnail loading image invisible
			var thumbnailContainer:Canvas=e.target.parent as Canvas;
			var thumbnailLoadingImage:Image=thumbnailContainer.getChildAt(1) as Image;
			thumbnailLoadingImage.visible=false;




		}



		public function LoadBigImageStart(allThumbnailContainersIndex:Number):void
		{
			var thumbnailData:ThumbnailData=LoadThumbnailData(allThumbnailContainersIndex);
			var source:String=_imagesPath + thumbnailData.ImageName;
			_currentThumbnailData=thumbnailData;
			
			//set busy cursor and show invisible mask to prevent user from clicking any more buttons
			CursorManager.setBusyCursor();
_mainStage.FullScreenInvisibleMask.visible = true;

			//create event listener to indicate when image finished downloading
			var urlRequest:URLRequest=new URLRequest(source);
			_imageLoader.load(urlRequest);
			_imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, LoadBigImageEnd);
			_imageLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, LoadProgressListener);

			_mainStage.PercentLoaded.text="0%";
			_mainStage.Preloader.visible=true;


		/* _nextImage.addEventListener(HTTPStatusEvent.HTTP_STATUS, LoadImageEnd);
		 _nextImage.load(source); */



			 //CheckIfImageFirstOrLast();
		}

		private function LoadProgressListener(e:ProgressEvent):void
		{
			var numberFormatter:NumberFormatter=new NumberFormatter();
			numberFormatter.precision=0;
			_mainStage.PercentLoaded.text=(numberFormatter.format(e.bytesLoaded / e.bytesTotal * 100)) + "%";
		}

		private function LoadBigImageEnd(e:Event):void
		{
			//Next image finished downloading, fade out image swap with big image
			CursorManager.removeBusyCursor();
			_mainStage.FullScreenInvisibleMask.visible = false;
			_mainStage.Preloader.visible=false;
			_fadeInImage.end();
			_fadeOutImage.play([_mainStage.BigImage]);
			setTimeout(SwapImage, _fadeOutImage.duration);
			_mainStage.ImageCaption.text=_currentThumbnailData.Caption;
			_mainStage.ImageCounter.text=(_currentThumbnailData.Index + 1).toString() + "/" + _totalThumbnails;

			//update url to refelct currrent location
			_navigationController.updateURL(_currentCategory, _currentThumbnailData.Guid, _currentThumbnailData.Caption);

			//reset other thumbnail borders and make active thumbnail border white
			ResetThumbnailBorders();
			var currentThumbnailContainer:Canvas=_allThumbnailContainsers[_currentThumbnailData.Index];
			currentThumbnailContainer.setStyle("backgroundColor", "#ffffff");

			//set active button 
			_navigationController.ResetAllFooterNavigationButtons();
			switch (_currentCategory)
			{
				case MenuItems.Advertising:
					_mainStage.AdvertisingButtonFooter.setStyle("color", _navigationController._textRollOverColor);
					_mainStage.AdvertisingButtonFooter.removeEventListener(MouseEvent.MOUSE_OUT, _navigationController.FooterTextMouseOut);
					_mainStage.AdvertisingButtonFooter.removeEventListener(MouseEvent.MOUSE_OVER, _navigationController.FooterTextMouseOver);
					break;
					case MenuItems.RecentWork:
					_mainStage.RecentWorkButtonFooter.setStyle("color", _navigationController._textRollOverColor);
					_mainStage.RecentWorkButtonFooter.removeEventListener(MouseEvent.MOUSE_OUT, _navigationController.FooterTextMouseOut);
					_mainStage.RecentWorkButtonFooter.removeEventListener(MouseEvent.MOUSE_OVER, _navigationController.FooterTextMouseOver);
					break;
				case MenuItems.Archive:
					_mainStage.ArchiveButtonFooter.setStyle("color", _navigationController._textRollOverColor);
					_mainStage.ArchiveButtonFooter.removeEventListener(MouseEvent.MOUSE_OUT, _navigationController.FooterTextMouseOut);
					_mainStage.ArchiveButtonFooter.removeEventListener(MouseEvent.MOUSE_OVER, _navigationController.FooterTextMouseOver);
					break;
				case MenuItems.Editorial:
					_mainStage.EditorialButtonFooter.setStyle("color", _navigationController._textRollOverColor);
					_mainStage.EditorialButtonFooter.removeEventListener(MouseEvent.MOUSE_OUT, _navigationController.FooterTextMouseOut);
					_mainStage.EditorialButtonFooter.removeEventListener(MouseEvent.MOUSE_OVER, _navigationController.FooterTextMouseOver);
					break;
			/* 	case MenuItems.Complete:
					_mainStage.CompleteButtonFooter.setStyle("color", _navigationController._textRollOverColor);
					_mainStage.CompleteButtonFooter.removeEventListener(MouseEvent.MOUSE_OUT, _navigationController.TextMouseOut);
					_mainStage.CompleteButtonFooter.removeEventListener(MouseEvent.MOUSE_OVER, _navigationController.TextMouseOver);
					break; */
				case MenuItems.StillLifes:
					_mainStage.StillLifesButtonFooter.setStyle("color", _navigationController._textRollOverColor);
					_mainStage.StillLifesButtonFooter.removeEventListener(MouseEvent.MOUSE_OUT, _navigationController.FooterTextMouseOut);
					_mainStage.StillLifesButtonFooter.removeEventListener(MouseEvent.MOUSE_OVER, _navigationController.FooterTextMouseOver);
					break;
			}

		}

		public function ResetThumbnailBorders():void
		{
			//make all thumbnail borders black
			var thumbnailContainer:Canvas;
			var allThumbnailContainers:Array=_mainStage.MiddleMenuThumbnailImages.getChildren();
			for each (thumbnailContainer in allThumbnailContainers)
			{
				thumbnailContainer.setStyle("backgroundColor", "#000000");

			}
		}


		private function SwapImage():void
		{
			//swap next image with main image and fade in main image
			_mainStage.BigImage.load(_imageLoader.content);
			_mainStage.BigImage.width=_navigationController._imageWidth;
			_mainStage.BigImage.height=_navigationController._imageHeight;
			_navigationController.OnResizeElements();
			_mainStage.BigImage.visible=true;
			_fadeInImage.play([_mainStage.BigImage]);
		}

		public function NextImage():void
		{
			if (_currentThumbnailData.Index < _totalThumbnails - 1)
			{
				LoadBigImageStart(_currentThumbnailData.Index + 1);

			}
			else
			{
				LoadBigImageStart(0);

			}
			//CheckIfImageFirstOrLast();

		}

		public function PreviousImage():void
		{
			if (_currentThumbnailData.Index > 0)
			{
				LoadBigImageStart(_currentThumbnailData.Index - 1);

			}
			else
			{
				LoadBigImageStart(_totalThumbnails - 1);

			}
			//CheckIfImageFirstOrLast();


		}

//		private function CheckIfImageFirstOrLast():void
//		{
//			if (_currentThumbnailData.Index == 0)
//			{
//				_mainStage.PreviousImageButton.setStyle("Color", _navigationController._textRollOverColor);
//				_mainStage.PreviousImageButton.enabled=false;
//			}
//			else
//			{
//
//				_mainStage.PreviousImageButton.setStyle("Color", "#ffffff");
//				_mainStage.PreviousImageButton.enabled=true;
//			}
//
//			if (_currentThumbnailData.Index == _totalThumbnails - 1)
//			{
//				_mainStage.NextImageButton.setStyle("Color", _navigationController._textRollOverColor);
//				_mainStage.NextImageButton.enabled=false;
//
//			}
//			else
//			{
//				_mainStage.NextImageButton.setStyle("Color", "#ffffff");
//				_mainStage.NextImageButton.enabled=true;
//			}
//
//
//		}



	}
}