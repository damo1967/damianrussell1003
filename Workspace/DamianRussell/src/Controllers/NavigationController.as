package Controllers
{

	import Helpers.*;

	import flash.display.*;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.utils.*;

	import mx.controls.ColorPicker;
	import mx.controls.Image;
	import mx.controls.Text;
	import mx.core.Application;
	import mx.effects.Fade;
	import mx.effects.Move;
	import mx.effects.easing.*;
	import mx.events.BrowserChangeEvent;
	import mx.managers.BrowserManager;
	import mx.managers.IBrowserManager;
	import mx.utils.URLUtil;



	public class NavigationController
	{
		private var _mainStage:DamianRussell;
		private var _stage:Stage;

		private var _fadeInPanels:Fade=new Fade();
		private var _fadeOutPanels:Fade=new Fade();
		private var _fadeOverText:Fade=new Fade();
		private var _fadeOutText:Fade=new Fade();

		public var _fadeInMenu:Fade=new Fade();

		public var _fadeOutMenu:Fade=new Fade();


		public var _textRollOver:AnimateColor=new AnimateColor();

		public var _textRollOff:AnimateColor=new AnimateColor();

		public var _startColor:ColorPicker=new ColorPicker();

		public var _stopColor:ColorPicker=new ColorPicker();

		public var _footerRollOver:AnimateColor=new AnimateColor();

		public var _footerRollOff:AnimateColor=new AnimateColor();
//private var _textMouseOverFadeOut:Fade = new Fade();
//private var _textMouseOutFadeIn:Fade = new Fade();



		private var _moveThumbnails:Move=new Move();

		public var _galleryController:GalleryController;

		public var _infoPagesController:InfoPagesController;

		private var _xMLEditorialData:XML;
		private var _xMLStillLifeData:XML;
		private var _xMLAdvertisingData:XML;
		private var _xMLArchiveData:XML;
		private var _xMLRecentWorkData:XML;
		private var _numberOfXMLFilesReceived:int=0;

		private var _xMLHelpers:XMLHelpers=new XMLHelpers(this);
		private var _xMLReceived:Boolean=false; //are all the xml files finished downloading

		public var _imageWidth:Number=1600;

		public var _imageHeight:Number=1050;

		public var _imageRatio:Number=_imageWidth / _imageHeight;
		//private var _minimumScreenSize:Number = 630;

		public var _finishedInitialising:Boolean=false;

		private var _scrollThumbnailTimer:int;

		public var _lengthOfThumbnailImages:int=0;

		private var _slideShowRunning:Boolean=false;
		private var _slideShowSetInterval:int;

		public var _textRollOverColor:String="#ffffff";

		public var _textRollOffColor:String="#666666";

		//Deeplinking
		//local reference to the singleton class BrowserManager

		public var browserManager:IBrowserManager;

//flag to signal that a URL parsing is in progress
		private var isParsing:Boolean;


		public var _currentImageGuid:String;


		private var _textButtonOriginalColor:String; //var to hold the orginal color of a text button before the rollover, to prevent resetting color to normal if active

		public function NavigationController()
		{
			//	_stage = stage;
		}

		public function init():void
		{
			//grab the application object
			_mainStage=Application.application as DamianRussell;
			_stage=_mainStage.stage;
			_galleryController=new GalleryController(this._mainStage, this);
			_infoPagesController=new InfoPagesController(this._mainStage, this);

			//initialise fades
			_fadeInPanels.alphaTo=1;
			_fadeInPanels.duration=1500;
			_fadeOutPanels.alphaTo=0;
			_fadeOutPanels.duration=1500;

			_fadeInMenu.alphaTo=1;
			_fadeInMenu.duration=500;
			_fadeOutMenu.alphaTo=0;
			_fadeOutMenu.duration=500;

			_fadeOverText.alphaFrom=1;
			_fadeOverText.alphaTo=0.5;
			_fadeOverText.duration=500;
			_fadeOutText.alphaFrom=0.5;
			_fadeOutText.alphaTo=1;
			_fadeOutText.duration=500;

			_startColor.selectedColor=0x666666;
			_stopColor.selectedColor=0xffffff;

	
			_textRollOver.fromValue=_startColor.selectedColor;
			_textRollOver.toValue=_stopColor.selectedColor;
			_textRollOver.duration=500;
			_textRollOver.isStyle=false;

		
			_textRollOff.fromValue=_stopColor.selectedColor;
			_textRollOff.toValue=_startColor.selectedColor;
			_textRollOff.duration=500;
			_textRollOff.isStyle=false;

			_footerRollOver.property="color";
			_footerRollOver.fromValue=_startColor.selectedColor;
			_footerRollOver.toValue=_stopColor.selectedColor;
			_footerRollOver.duration=500;
			_footerRollOver.isStyle=true;

			_footerRollOff.property="color";
			_footerRollOff.fromValue=_stopColor.selectedColor;
			_footerRollOff.toValue=_startColor.selectedColor;
			_footerRollOff.duration=500;
			_footerRollOff.isStyle=true ;



//			_textMouseOverFadeOut.alphaTo=0.5;
//			_textMouseOverFadeOut.duration=1000;
//			_textMouseOverFadeOut.duration=200;
//			_textMouseOutFadeIn.alphaTo=1;
//			_textMouseOutFadeIn.duration=200;

			_moveThumbnails.target=_mainStage.MiddleMenuThumbnailImagesScroll;

//			//open splash panel
//			SplashPanelFadeIn();

			//begin async read xml file
			_xMLReceived=false;
			_xMLHelpers.ReadEditorialXMLAsynch();
			_xMLHelpers.ReadStillLifeXMLAsynch();
			_xMLHelpers.ReadAdvertisingXMLAsynch();
			_xMLHelpers.ReadArchiveXMLAsynch();
			_xMLHelpers.ReadRecentWorkXMLAsynch();
			
			//set rollover states on to text buttons
			ResetAllFooterNavigationButtons();

				//var _allFooterButtons:Array=[_mainStage.SplashText, _mainStage.EditorialButton, _mainStage.StillLifesButton, _mainStage.AdvertisingButton, _mainStage.ArchiveButton, _mainStage.BiographyButton, _mainStage.ContactsButton, _mainStage.ClientsButton];
			//	var textButton:Text;

			//attach rollover behaviours to text images
			var allImageButtons:Array=[_mainStage.SplashText, _mainStage.EditorialButton, _mainStage.StillLifesButton, _mainStage.AdvertisingButton,  _mainStage.RecentWorkButton, _mainStage.ArchiveButton, _mainStage.BiographyButton, _mainStage.ContactsButton, _mainStage.ClientsButton];
			var imageButton:Image;
			for each (imageButton in allImageButtons)
			{
				imageButton.addEventListener(MouseEvent.MOUSE_OVER, TextMouseOver);
				imageButton.addEventListener(MouseEvent.MOUSE_OUT, TextMouseOut);
			}

			//	for each (textButton in _allTextButtons)
			//	{
			//		textButton.addEventListener(MouseEvent.MOUSE_OVER, TextMouseOver);
			//		textButton.addEventListener(MouseEvent.MOUSE_OUT, TextMouseOut);
			//	}

//deeplinking init
			browserManager=BrowserManager.getInstance();
			browserManager.init("", "Damian Russell Photographer");
			browserManager.addEventListener(BrowserChangeEvent.BROWSER_URL_CHANGE, parseURL);
			parseURL();

			_mainStage.MenusCloseTriggerTop.height=CalculateMenusCloseTriggerTopHeight();
			_mainStage.MenusCloseTriggerBottom.height=CalculateMenusCloseTriggerBottomHeight();

			_finishedInitialising=true;
		}

		public function SetXMLContents(xMLContents:XML, category:String):void
		{
			switch (category)
			{
				case MenuItems.Editorial:
					//receive asyn result
					_xMLEditorialData=xMLContents;
					_numberOfXMLFilesReceived++;
					break;

				case MenuItems.StillLifes:
					//receive asyn result
					_xMLStillLifeData=xMLContents;
					_numberOfXMLFilesReceived++;
					break;

				case MenuItems.Advertising:
					//receive asyn result
					_xMLAdvertisingData=xMLContents;
					_numberOfXMLFilesReceived++;
					break;

				case MenuItems.Archive:
					//receive asyn result
					_xMLArchiveData=xMLContents;
					_numberOfXMLFilesReceived++;
					break;
					
				case MenuItems.RecentWork:
					//receive asyn result
					_xMLRecentWorkData=xMLContents;
					_numberOfXMLFilesReceived++;
					break;
			}

			if (_numberOfXMLFilesReceived >= _xMLHelpers.NumberOfXMLFiles) //all the xml files have been received
			{
				_xMLReceived=true
			}
		}

		/**
		 *
		 */
		public function SplashPanelFadeIn():void
		{
			_mainStage.SplashPanel.visible=true;
			_mainStage.SplashText.visible=true;
			_fadeInPanels.play([_mainStage.SplashText]);
		}

		/**
		 *
		 */
		public function MenuPanelFadeIn():void
		{
			//fade in menu
			_mainStage.MenuPanel.includeInLayout=true;
			_mainStage.MenuPanel.visible=true;
			_mainStage.MenuPanel.alpha=1;

			_fadeInPanels.play([_mainStage.EditorialButton, _mainStage.StillLifesButton, _mainStage.AdvertisingButton, _mainStage.RecentWorkButton, _mainStage.ArchiveButton, _mainStage.BiographyButton, _mainStage.ContactsButton, _mainStage.CopyrightNotice, _mainStage.ClientsButton, _mainStage.MenuHR1, _mainStage.MenuHR2]);

			//make footer visible
			_mainStage.Footer.visible=true;

			//update url to refelct currrent location
			updateURL(MenuItems.Menu, "None", "Menu");


		}

		/* 	public function MenuPanelFadeOut():void
		   {
		   //fade in menu
		   _fadeInMiddleMenu.end()
		   _fadeOutMiddleMenu.play([_mainStage.MenuPanel]);


		 } */

		private function GalleryPanelInitialise():void
		{
			_mainStage.GallerysPanel.visible=true;
			_mainStage.GallerysPanel.includeInLayout=true;
			_mainStage.GallerysPanel.alpha=1;

			_mainStage.BigImage.visible=true;
			_mainStage.MiddleMenuShape.visible=true;

		}



		/**
		 *
		 */
		public function EditorialPanelFadeIn():void
		{
			GalleryPanelInitialise();

			_galleryController.InitialiseGallary(_xMLEditorialData, MenuItems.Editorial);


		}

		/**
		 *
		 */
		public function StillLifesPanelFadeIn():void
		{
			GalleryPanelInitialise();

			_galleryController.InitialiseGallary(_xMLStillLifeData, MenuItems.StillLifes);
		}

		/**
		 *
		 */
		public function AdvertisingPanelFadeIn():void
		{
			GalleryPanelInitialise();

			_galleryController.InitialiseGallary(_xMLAdvertisingData, MenuItems.Advertising);

		}
/**
		 *
		 */
		public function RecentWorkPanelFadeIn():void
		{
			GalleryPanelInitialise();

			_galleryController.InitialiseGallary(_xMLRecentWorkData, MenuItems.RecentWork);

		}
		
		/**
		 *
		 */
		public function ArchivePanelFadeIn():void
		{
			GalleryPanelInitialise();

			_galleryController.InitialiseGallary(_xMLArchiveData, MenuItems.Archive);

		}

		/*  		public function CompletePanelFadeIn():void
		   {
		   GalleryPanelInitialise();
		   _galleryController.InitialiseGallary(_xMLData, MenuItems.Complete);
		   }
		 */
		/**
		 *
		 */
		public function BiographyPanelFadeIn():void
		{

			_infoPagesController.InitialiseInfoPages();
			_infoPagesController.LoadInfoPagesBackgroundImageStart(MenuItems.Biography);
			//GalleryPanelInitialise();
			//_galleryController.InitialiseGallary(_xMLData, MenuItems.Complete);
		}

		/**
		 *
		 */
		public function ContactsPanelFadeIn():void
		{
			_infoPagesController.InitialiseInfoPages();
			_infoPagesController.LoadInfoPagesBackgroundImageStart(MenuItems.Contacts);
		}

		/**
		 *
		 */
		public function ClientsPanelFadeIn():void
		{
			_infoPagesController.InitialiseInfoPages();
			_infoPagesController.LoadInfoPagesBackgroundImageStart(MenuItems.Clients);
		}

		/**
		 *
		 */
		public function MiddleMenuFadeIn():void
		{

			_fadeOutMenu.end();
			if (_mainStage.MiddleMenu.alpha < 0.5)
			{
				_mainStage.MiddleMenu.visible=true;
				_fadeInMenu.play([_mainStage.MiddleMenu]);


			}

		}


		/**
		 *
		 */
		public function MiddleMenuFadeOut():void
		{

			_fadeInMenu.end();
			if (_mainStage.MiddleMenu.alpha > 0.5)
			{
				_fadeOutMenu.play([_mainStage.MiddleMenu]);

			}

		}


		/**
		 *
		 */
		public function AllMenusFadeIn():void
		{
			_mainStage.MenusCloseTriggerTop.visible=true;
			_mainStage.MenusCloseTriggerBottom.visible=true;
			_fadeOutMenu.end();
			/* 			_blurOutBigPicture.end();
			 _blurInBigPicture.play([_mainStage.BigImage]); */
			//_mainStage.BigImage.filters=[_blurInBigPicture];
			if (_mainStage.Footer.alpha < 0.5)
			{
				_fadeInMenu.play([_mainStage.Footer]);

				if (_mainStage.MiddleMenu.visible)
				{
					MiddleMenuFadeIn();
				}
			}

		}

		/**
		 *
		 */
		public function AllMenusFadeOut():void
		{
			_fadeInMenu.end();
			_fadeOutMenu.play([_mainStage.Footer]);
			_mainStage.MenusCloseTriggerTop.visible=false;
			_mainStage.MenusCloseTriggerBottom.visible=false;

			/* 	_blurInBigPicture.end();
			 _blurOutBigPicture.play([_mainStage.BigImage]); */

			if (_mainStage.MiddleMenu.visible)
			{
				MiddleMenuFadeOut()
			}
			//setInterval(FooterInvisible,_fadeOutMenu.duration);

		}

		private function ResetAllPanels():void
		{
			//clear all other panels
			FadeOutAllPanels();
			setTimeout(HideAllPanels, _fadeOutPanels.duration);
		}

		/**
		 *
		 * @param panelToOpen
		 * @param imageGuid
		 */
		public function PanelSwitchboard(panelToOpen:String, imageGuid:String):void
		{
			_currentImageGuid=imageGuid;

			//open relevant panel
			switch (panelToOpen)
			{
				case MenuItems.Splash:
					//open splash panel
					SplashPanelFadeIn();
					break;
				case MenuItems.Menu:
					ResetAllPanels()
					_fadeOutPanels.play([_mainStage.SplashText]);
					setTimeout(MenuPanelFadeIn, _fadeOutPanels.duration);
					break;
				/* 	case MenuItems.Complete:
				   ResetAllPanels()
				   setTimeout(CompletePanelFadeIn, _fadeOutPanels.duration);
				 break; */
				case MenuItems.Editorial:
					ResetAllPanels()
					setTimeout(EditorialPanelFadeIn, _fadeOutPanels.duration);
					break;
				case MenuItems.StillLifes:
					ResetAllPanels()
					setTimeout(StillLifesPanelFadeIn, _fadeOutPanels.duration);
					break;
				case MenuItems.Advertising:
					ResetAllPanels()
					setTimeout(AdvertisingPanelFadeIn, _fadeOutPanels.duration);
					break;
				case MenuItems.Archive:
					ResetAllPanels()
					setTimeout(ArchivePanelFadeIn, _fadeOutPanels.duration);
					break;
				case MenuItems.Biography:
					ResetAllPanels()
					setTimeout(BiographyPanelFadeIn, _fadeOutPanels.duration);
					break;
				case MenuItems.Contacts:
					ResetAllPanels()
					setTimeout(ContactsPanelFadeIn, _fadeOutPanels.duration);
					break;
				case MenuItems.Clients:
					ResetAllPanels()
					setTimeout(ClientsPanelFadeIn, _fadeOutPanels.duration);
					break;
				case MenuItems.RecentWork:
					ResetAllPanels()
					setTimeout(RecentWorkPanelFadeIn, _fadeOutPanels.duration);
					break;
				default:
					//open splash panel
					SplashPanelFadeIn();
					break;


			}


		}






		/**
		 *
		 */
		public function FadeOutAllPanels():void
		{

			if (_mainStage.SplashPanel.visible)
				_fadeOutPanels.play([_mainStage.SplashText]);
			if (_mainStage.MenuPanel.visible)
				_fadeOutPanels.play([_mainStage.EditorialButton, _mainStage.StillLifesButton, _mainStage.AdvertisingButton, _mainStage.RecentWorkButton, _mainStage.ArchiveButton, _mainStage.BiographyButton, _mainStage.ContactsButton, _mainStage.CopyrightNotice, _mainStage.ClientsButton, _mainStage.MenuHR1, _mainStage.MenuHR2]);
			if (_mainStage.GallerysPanel.visible)
				_fadeOutPanels.play([_mainStage.BigImage, _mainStage.MiddleMenuShape]);
			if (_mainStage.InfoPagesPanel.visible)
				_fadeOutPanels.play([_mainStage.InfoPagesBackgroundImage, _mainStage.BiographyPage, _mainStage.BiographyPicture, _mainStage.ContactPage, _mainStage.ContactHotSpot1, _mainStage.ContactHotSpot2, _mainStage.ClientsPage]);


		}

		/**
		 *
		 */
		public function HideAllPanels():void
		{
			_mainStage.SplashPanel.includeInLayout=false;
			_mainStage.MenuPanel.includeInLayout=false;
			_mainStage.GallerysPanel.includeInLayout=false;
			_mainStage.InfoPagesPanel.includeInLayout=false;

			_mainStage.SplashPanel.visible=false;
			_mainStage.MenuPanel.visible=false;
			_mainStage.GallerysPanel.visible=false;
			_mainStage.InfoPagesPanel.visible=false;


		}



		//  ************  Resize background  *************************


		/**
		 *
		 */
		public function OnResizeElements():void
		{
			//resize picture
			if (_finishedInitialising == true)
			{
				var screenRatio:Number=_mainStage.width / _mainStage.height;
				//set size of background image
				if (screenRatio > _imageRatio)
				{
					_mainStage.BigImage.width=_mainStage.BigImage.width * (_mainStage.height / _mainStage.BigImage.height);
					_mainStage.BigImage.height=_mainStage.height;

					_mainStage.InfoPagesBackgroundImage.width=_mainStage.InfoPagesBackgroundImage.width * (_mainStage.height / _mainStage.InfoPagesBackgroundImage.height);
					_mainStage.InfoPagesBackgroundImage.height=_mainStage.height;

//			
				}
				else
				{
					_mainStage.BigImage.height=_mainStage.BigImage.height * (_mainStage.width / _mainStage.BigImage.width);
					_mainStage.BigImage.width=_mainStage.width;

					_mainStage.InfoPagesBackgroundImage.height=_mainStage.InfoPagesBackgroundImage.height * (_mainStage.width / _mainStage.InfoPagesBackgroundImage.width);
					_mainStage.InfoPagesBackgroundImage.width=_mainStage.width;
//			
				}

//set minimum size for infoPagesBackgroundImage
				if (_mainStage.InfoPagesBackgroundImage.width * (_mainStage.height / _mainStage.InfoPagesBackgroundImage.height) < 800)
				{
					_mainStage.InfoPagesBackgroundImage.width=800;
					_mainStage.InfoPagesBackgroundImage.height=528;
				}



				//set position of background picture
				if (_mainStage.height > _mainStage.BigImage.height)
				{
					_mainStage.BigImage.y=(_mainStage.height - _mainStage.BigImage.height) / 2;
					_mainStage.InfoPagesBackgroundImage.y=(_mainStage.height - _mainStage.InfoPagesBackgroundImage.height) / 2;
				}
				else
				{
					_mainStage.BigImage.y=0
					_mainStage.InfoPagesBackgroundImage.y=0
				}

				if (_mainStage.width > _mainStage.BigImage.width)
				{

					_mainStage.BigImage.x=(_mainStage.width - _mainStage.BigImage.width) / 2;
					_mainStage.InfoPagesBackgroundImage.x=(_mainStage.width - _mainStage.InfoPagesBackgroundImage.width) / 2;
				}
				else
				{
					_mainStage.BigImage.x=0
					_mainStage.InfoPagesBackgroundImage.x=0
				}

				//resize menu close triggers
				_mainStage.MenusCloseTriggerTop.height=CalculateMenusCloseTriggerTopHeight();
				_mainStage.MenusCloseTriggerBottom.height=CalculateMenusCloseTriggerBottomHeight();

				//show or hide thumbnail buttons
				if (_mainStage.MiddleMenuThumbnailImagesScroll.x > -1 * (_lengthOfThumbnailImages - _mainStage.MiddleMenuThumbnails.width))
				{
					_mainStage.MiddleMenuThumbnailsRightButton.visible=true;
				}
				else
				{

					_mainStage.MiddleMenuThumbnailsRightButton.visible=false;
				}

				if (_mainStage.MiddleMenuThumbnailImagesScroll.x < 0)
				{
					_mainStage.MiddleMenuThumbnailsLeftButton.visible=true;
				}
				else
				{
					_mainStage.MiddleMenuThumbnailsLeftButton.visible=false;

				}

				//change text of Full Screen button to accomodate user pushing escape to close fullscreen
				if (_stage.displayState == StageDisplayState.FULL_SCREEN)
				{
					_mainStage.FullScreenButtonFooter.text="View Normal Screen";

				}
				else
				{
					_mainStage.FullScreenButtonFooter.text="View Full Screen";


				}


				/* 	 	//prevent menus from getting too small
				   if (_mainStage.width < _minimumScreenSize)
				   {
				   _mainStage.MiddleMenuTopText.width = _minimumScreenSize;
				   _mainStage.FooterMenu.width = _minimumScreenSize;
				   }
				   else
				   {
				   _mainStage.MiddleMenuTopText.width = _mainStage.width;
				   _mainStage.FooterMenu.width = _mainStage.width;
				 }  */

			}
		}

		/**
		 *
		 * @return
		 */
		public function CalculateMenusCloseTriggerTopHeight():Number
		{
			return (_mainStage.height - _mainStage.MiddleMenuTopText.height - _mainStage.MiddleMenuThumbnails.height - _mainStage.FooterMenu.height) / 2 + 8;

		}

		/**
		 *
		 * @return
		 */
		public function CalculateMenusCloseTriggerBottomHeight():Number
		{
			return (_mainStage.height - _mainStage.MiddleMenuTopText.height - _mainStage.MiddleMenuThumbnails.height - _mainStage.FooterMenu.height) / 2 - 16;
		}

		//  ***********          Thumbnails Scrolling

		/**
		 *
		 */
		public function ScrollThumbnailsLeftBegin():void
		{

			//_moveThumbnails.easingFunction=mx.effects.easing.Linear.easeNone;
			_scrollThumbnailTimer=setInterval(ScrollThumbnailsLeft, 40);



			//_scrollThumbnailTimer=setInterval(ScrollThumbnailsLeft, 30);

		}

		/**
		 *
		 */
		public function ScrollThumbnailsLeftEnd():void
		{
			clearInterval(_scrollThumbnailTimer);

		}

		/**
		 *
		 */
		public function ScrollThumbnailsLeft():void
		{
			if (_mainStage.MiddleMenuThumbnailImagesScroll.x > -1 * (_lengthOfThumbnailImages - _mainStage.MiddleMenuThumbnails.width))
			{
				_mainStage.MiddleMenuThumbnailImagesScroll.x-=4;
				_mainStage.MiddleMenuThumbnailsLeftButton.visible=true;
			}
			else
			{
				_mainStage.MiddleMenuThumbnailsRightButton.visible=false;

			}
//					_moveThumbnails.end();
//                _moveThumbnails.xBy=-1; 
//                _moveThumbnails.play();

		}


		/**
		 *
		 */
		public function ScrollThumbnailsRightBegin():void
		{

			_scrollThumbnailTimer=setInterval(ScrollThumbnailsRight, 40);

		}

		/**
		 *
		 */
		public function ScrollThumbnailsRightEnd():void
		{
			clearInterval(_scrollThumbnailTimer);
		}

		/**
		 *
		 */
		public function ScrollThumbnailsRight():void
		{
			if (_mainStage.MiddleMenuThumbnailImagesScroll.x < 0)
			{
				_mainStage.MiddleMenuThumbnailsRightButton.visible=true;
				_mainStage.MiddleMenuThumbnailImagesScroll.x+=4;
			}
			else
			{
				_mainStage.MiddleMenuThumbnailsLeftButton.visible=false;

			}
//			_moveThumbnails.end();
//                _moveThumbnails.xBy=1; 
//                _moveThumbnails.play();



		}

		/**
		 *
		 */
		public function FullScreenFlipSwitch():void
		{
			//toggle the display state 
			//the text for the toggle button is controller by the OnResizeElements function
			if (_stage.displayState == StageDisplayState.FULL_SCREEN)
			{

				_stage.displayState=StageDisplayState.NORMAL;
			}
			else
			{

				_stage.displayState=StageDisplayState.FULL_SCREEN;

			}




		}

		/**
		 *
		 */
		public function SlideShowFlipSwitch():void
		{
			if (_slideShowRunning)
			{
				_slideShowRunning=false;
				clearInterval(_slideShowSetInterval);
				_mainStage.SlideShowButtonFooter.text="Start Slide Show";


			}
			else
			{
				_slideShowRunning=true;
				_slideShowSetInterval=setInterval(SlideShowNextImage, 5000)
				_mainStage.SlideShowButtonFooter.text="Stop Slide Show";

			}


		}

		private function SlideShowNextImage():void
		{
			if (_galleryController._currentThumbnailData.Index < (_galleryController._totalThumbnails - 1))
			{
				_galleryController.LoadBigImageStart(_galleryController._currentThumbnailData.Index + 1);

			}
			else
			{
				_galleryController.LoadBigImageStart(0);

			}


		}


		public function TextMouseOver(e:MouseEvent):void
		{
			//_textMouseOutFadeIn.end();
			var image:Image=e.target as Image;

//_textRollOver.end();
			_textRollOver.target=image;
			_textRollOver.play();

			//		_fadeOutText.end();


			//	_fadeOverText.play([text]);

		}


		public function TextMouseOut(e:MouseEvent):void
		{
			var image:Image=e.target as Image;
			//	_fadeOverText.end();
			//	_fadeOutText.play([text]);

//_textRollOff.end();
			_textRollOff.target=image;
			_textRollOff.play();


		}

		public function FooterTextMouseOver(e:MouseEvent):void
		{
			//_textMouseOutFadeIn.end();
			var text:Text=e.target as Text;

//_textRollOver.end();
			_footerRollOver.target=text;
			_footerRollOver.play();

			//		_fadeOutText.end();


			//	_fadeOverText.play([text]);

		}


		public function FooterTextMouseOut(e:MouseEvent):void
		{
			var text:Text=e.target as Text;
			//	_fadeOverText.end();
			//	_fadeOutText.play([text]);

//_textRollOff.end();
			_footerRollOff.target=text;
			_footerRollOff.play();


		}


		/**
		 * This is the event listener called whenever the browser URL changes.
		 * @param event
		 */
		private function parseURL(event:Event=null):void
		{
			isParsing=true;
			//retrieve the current fragmens from the browser URL and parse them into an Object;
			//eg if we had view=1 we will have an object with the propery “view” and value “1″
			var o:Object=URLUtil.stringToObject(browserManager.fragment);
			//if it is the default state URL;
			if (o.category == undefined)
				o.category=MenuItems.Splash;
			if (o.imageGuid == undefined)
				o.imageGuid="None";
			//set the application to play the category from the URL 

			PanelSwitchboard(o.category, o.imageGuid);
//    //set the title displayed by the browser to the title coresponding to accordion selection
//    var title:String = arrTitles[o.view];
//    browserManager.setTitle(title);
			isParsing=false;
		}

		/**
		 * Event listener registered for changes
		 */
		public function updateURL(category:String, imageGuid:String, title:String):void
		{
			//if we are not doing a parsing of URL
			if (!isParsing)
				//callLater postpones the execution of the given function until the next frame;
				//thus the UI has a chance to be created;
				doUpdateURL(category, imageGuid, title);
		}

		/**
		 * This function modifies the browser URL.
		 */
		private function doUpdateURL(category:String, imageGuid:String, title:String):void
		{


			var fragments:String="";
			var o:Object={};
			//if the user selected other step of the accordion than the first one
			// if (ac.selectedIndex > 0)
			o.category=category;
			o.imageGuid=imageGuid;
			//retrieve the title for the selected state   
			title="Damian Russell : " + title;
			//serialize the object to fragmenst string
			fragments=URLUtil.objectToString(o);
			//sets the title of the browser
			browserManager.setTitle(title);
			//sets the fragments to the browser URL
			browserManager.setFragment(fragments);
		}

		public function GoToCMSMenu():void
		{
			var url:String="Admin/AdminMenu.aspx";
			var request:URLRequest=new URLRequest(url);

			navigateToURL(request, '_self'); // second argument is target


		}

		public function ResetAllFooterNavigationButtons():void
		{
			/* 		_mainStage.CompleteButtonFooter.setStyle("color", _textRollOffColor);
			   _mainStage.CompleteButtonFooter.addEventListener(MouseEvent.MOUSE_OVER, TextMouseOver);
			 _mainStage.CompleteButtonFooter.addEventListener(MouseEvent.MOUSE_OUT, TextMouseOut); */

			_mainStage.EditorialButtonFooter.setStyle("color", _textRollOffColor);
			_mainStage.EditorialButtonFooter.addEventListener(MouseEvent.MOUSE_OVER, FooterTextMouseOver);
			_mainStage.EditorialButtonFooter.addEventListener(MouseEvent.MOUSE_OUT, FooterTextMouseOut);

			_mainStage.StillLifesButtonFooter.setStyle("color", _textRollOffColor);
			_mainStage.StillLifesButtonFooter.addEventListener(MouseEvent.MOUSE_OVER, FooterTextMouseOver);
			_mainStage.StillLifesButtonFooter.addEventListener(MouseEvent.MOUSE_OUT, FooterTextMouseOut);

			_mainStage.AdvertisingButtonFooter.setStyle("color", _textRollOffColor);
			_mainStage.AdvertisingButtonFooter.addEventListener(MouseEvent.MOUSE_OVER, FooterTextMouseOver);
			_mainStage.AdvertisingButtonFooter.addEventListener(MouseEvent.MOUSE_OUT, FooterTextMouseOut);

	_mainStage.RecentWorkButtonFooter.setStyle("color", _textRollOffColor);
			_mainStage.RecentWorkButtonFooter.addEventListener(MouseEvent.MOUSE_OVER, FooterTextMouseOver);
			_mainStage.RecentWorkButtonFooter.addEventListener(MouseEvent.MOUSE_OUT, FooterTextMouseOut);
			
			_mainStage.ArchiveButtonFooter.setStyle("color", _textRollOffColor);
			_mainStage.ArchiveButtonFooter.addEventListener(MouseEvent.MOUSE_OVER, FooterTextMouseOver);
			_mainStage.ArchiveButtonFooter.addEventListener(MouseEvent.MOUSE_OUT, FooterTextMouseOut);


			_mainStage.BiographyButtonFooter.setStyle("color", _textRollOffColor);
			_mainStage.BiographyButtonFooter.addEventListener(MouseEvent.MOUSE_OVER, FooterTextMouseOver);
			_mainStage.BiographyButtonFooter.addEventListener(MouseEvent.MOUSE_OUT, FooterTextMouseOut);


			_mainStage.ContactsButtonFooter.setStyle("color", _textRollOffColor);
			_mainStage.ContactsButtonFooter.addEventListener(MouseEvent.MOUSE_OVER, FooterTextMouseOver);
			_mainStage.ContactsButtonFooter.addEventListener(MouseEvent.MOUSE_OUT, FooterTextMouseOut);


			_mainStage.ClientsButtonFooter.setStyle("color", _textRollOffColor);
			_mainStage.ClientsButtonFooter.addEventListener(MouseEvent.MOUSE_OVER, FooterTextMouseOver);
			_mainStage.ClientsButtonFooter.addEventListener(MouseEvent.MOUSE_OUT, FooterTextMouseOut);

			_mainStage.SlideShowButtonFooter.setStyle("color", _textRollOffColor);
			_mainStage.SlideShowButtonFooter.addEventListener(MouseEvent.MOUSE_OVER, FooterTextMouseOver);
			_mainStage.SlideShowButtonFooter.addEventListener(MouseEvent.MOUSE_OUT, FooterTextMouseOut);

			_mainStage.FullScreenButtonFooter.setStyle("color", _textRollOffColor);
			_mainStage.FullScreenButtonFooter.addEventListener(MouseEvent.MOUSE_OVER, FooterTextMouseOver);
			_mainStage.FullScreenButtonFooter.addEventListener(MouseEvent.MOUSE_OUT, FooterTextMouseOut);

		}



	}




}









