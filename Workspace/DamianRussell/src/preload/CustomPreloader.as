package preload
{
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.ByteArray;
	import flash.utils.Timer;
	
	import mx.events.FlexEvent;
	import mx.preloaders.DownloadProgressBar;

	public class CustomPreloader extends DownloadProgressBar
	{
		private var timer:Timer;
		//progress bar 
		private var pBar:Sprite=new Sprite();
		//Progress bar mask 
		//private var maskBar:Sprite= new Sprite();
		//Text box to diplay loading percentage
		private var txtBox:TextField=new TextField();
		//loader for loading swf design of progress bar 
		private var loader:Loader;
		//Load swf file containing design of progress bar 
		[Embed(source="../../Content/Images/Loading.swf", mimeType="application/octet-stream")]
		public var WelcomeScreenGraphic:Class;
		//background color 
		private var bgSprite:Sprite=new Sprite();

		private var txtFormat:TextFormat;

		public function CustomPreloader()
		{
			//this.addChild(bgSprite);
			//timer
			timer=new Timer(1);
			timer.addEventListener(TimerEvent.TIMER, drawProgress);
			timer.start();
			//loading swf file of progress bar design
			loader=new Loader();
			loader.loadBytes(new WelcomeScreenGraphic() as ByteArray);

			pBar.addChild(loader);
			//this.addChild(loader);
			this.addChild(txtBox);



			//position textbox
			txtBox.textColor=0xffffff;
			txtFormat=new TextFormat();
			txtFormat.font="Arial";

		}

 private function SWFDownloadComplete(event : Event) : void {}
    
        private function FlexInitProgress(event : Event) : void {}
        
         private function FlexInitComplete(event : Event) : void 
        {      
           
            this.dispatchEvent(new Event(Event.COMPLETE));
        }

		override public function set preloader(preloader:Sprite):void
		{
			preloader.addEventListener(ProgressEvent.PROGRESS, SWFDownloadProgress);
			preloader.addEventListener(Event.COMPLETE,          SWFDownloadComplete);
            preloader.addEventListener(FlexEvent.INIT_PROGRESS, FlexInitProgress);
            preloader.addEventListener(FlexEvent.INIT_COMPLETE, FlexInitComplete);
            
            
            
			//position progressbar to center of stage
			var centerX:Number=(this.stage.stageWidth) / 2 - 28;
			var centerY:Number=(this.stage.stageHeight / 4);
			pBar.x=centerX;
			pBar.y=centerY;

			txtBox.x=centerX - 22;
			txtBox.y=centerY - 30;
//			bgSprite.width=this.stage.stageWidth;
//			bgSprite.height=this.stage.stageHeight;
			this.addChild(pBar);
		}
		private var progress:Number;

		private function SWFDownloadProgress(event:ProgressEvent):void
		{

			//progress multiplied by 2 cos our progress bar design is 200 px

			progress=Number(event.target.loaderInfo.bytesLoaded / event.target.loaderInfo.bytesTotal * 100);
		}
		private var currlen:Number=0;

		public function drawProgress(event:Event):void
		{
			//change the mask color to the color of your background
			if (currlen < progress)
			{
				currlen+=1;

				txtBox.text="Damian Russell\nLoading......" + Math.round(currlen) + "%";
				txtBox.setTextFormat(txtFormat);
			}
			if (currlen == 200)
			{
				timer.stop();
				dispatchEvent(new Event(Event.COMPLETE));
			}

		}

	}
}