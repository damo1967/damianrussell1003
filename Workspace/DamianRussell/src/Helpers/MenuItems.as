package Helpers
{

	public final class MenuItems
	{
		public static const Menu:String="Menu";
		public static const Editorial:String="Editorial";
		public static const StillLifes:String="Still Life";
		public static const Advertising:String="Advertising";
		public static const Complete:String="Complete";
		public static const Archive:String="Archive";
		public static const Biography:String="Biography";
		public static const Contacts:String="Contacts";
		public static const Clients:String="Clients";
		public static const Splash:String="Splash";
			public static const RecentWork:String="Recent Work";


	}
}