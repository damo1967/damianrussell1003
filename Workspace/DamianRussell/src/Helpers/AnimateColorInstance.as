package Helpers
{

import flash.geom.ColorTransform;

import mx.controls.Image;
import mx.effects.effectClasses.AnimatePropertyInstance;

/**
 * 
 */
public class AnimateColorInstance extends AnimatePropertyInstance
{
    /** The start color values for each of the r, g, and b channels */
    protected var startValues:Object;;
    
    /** The change in color value for each of the r, g, and b channels. */
    protected var delta:Object;
    
    /**
     * Constructor
     *
     * @param target The Object to animate with this effect.
     */
    public function AnimateColorInstance( target:Object )
    {
        super( target );
    }
    
    /**
     * @private
     */
    override public function play():void
    {
        // We need to call play first so that the fromValue is
        // correctly set, but this has the side effect of calling
        // onTweenUpdate before startValues or delta can be set,
        // so we need to check for that in onTweenUpdate to avoid
        // run time errors.
        super.play();
        
        // Calculate the delta for each of the color values
        startValues = ColorUtil.intToRgb( fromValue );
        var stopValues:Object = ColorUtil.intToRgb( toValue );
        delta = {
                    r: ( startValues.r - stopValues.r ) / duration,
                    g: ( startValues.g - stopValues.g ) / duration,
                    b: ( startValues.b - stopValues.b ) / duration
                };
        
    }
    
    /**
     * @private
     */
    override public function onTweenUpdate( value:Object ):void
    {
        // Bail out if delta hasn't been set yet
        if ( delta == null )
        {
            return;
        }
        
        // Catch the situation in which the playheadTime is actually more
        // than duration, which causes incorrect colors to appear at the 
        // end of the animation.
        var playheadTime:int = this.playheadTime;
        if ( playheadTime > duration )
        {
            // Fix the local playhead time to avoid going past the end color
            playheadTime = duration;
        }
        
        // Calculate the new color value based on the elapased time and the change
        // in color values
        var colorValue:int = ( ( startValues.r - playheadTime * delta.r ) << 16 )
                            + ( (startValues.g - playheadTime * delta.g ) << 8 )
                            + ( startValues.b - playheadTime * delta.b );
        
        // Either set the property directly, or set it as a style
        if ( !isStyle )
        {
        	//target[ property ] = colorValue;
            colorize(colorValue, target as Image);
        }
        else
        {
        	target.setStyle( property, colorValue );

        	
            
        }
    }
    
    public function colorize(value:Number, image:Image) :void
			{
				// Create a new ColorTransform instance.
				var colorTrans:ColorTransform = new ColorTransform();
				// Set the color value to be used by the ColorTransform instance.
				colorTrans.color = value;
				// Apply the transform to image.
				image.transform.colorTransform = colorTrans;
			}
    
} // end class
} // end package