package Helpers
{

import mx.effects.AnimateProperty;
import Helpers.AnimateColorInstance;
import mx.effects.IEffectInstance;    

/**
 * 
 */
public class AnimateColor extends AnimateProperty
{

    /**
     * Constructor
     *
     * @param target The Object to animate with this effect.
     */
    public function AnimateColor(target:Object = null)
    {
        super(target);
        
        instanceClass = AnimateColorInstance;
    }
    
    /**
     * @private
     */
    override protected function initInstance( instance:IEffectInstance ):void
    {
        super.initInstance( instance );
        
        var animateColorInstance:AnimateColorInstance = AnimateColorInstance( instance );

        animateColorInstance.fromValue = fromValue;
        animateColorInstance.toValue = toValue;
        animateColorInstance.property = property;
        animateColorInstance.isStyle = isStyle;
        animateColorInstance.roundValue = roundValue;
    }
    
} // end class    
} // end package