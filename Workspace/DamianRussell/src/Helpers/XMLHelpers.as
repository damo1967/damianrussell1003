package Helpers
{
import flash.events.*;
import flash.net.*;


	public class XMLHelpers
	{
	
		private var _callingRepository:Object = new Object();
		
				private var _xMLEditorialFile:String="XML/EditorialImages.xml";
private var _xMLStillLifeFile:String="XML/StillLifeImages.xml";
private var _xMLAdvertisingFile:String="XML/AdvertisingImages.xml";
private var _xMLArchiveFile:String="XML/ArchiveImages.xml";
private var _xMLRecentWorkFile:String="XML/RecentWorkImages.xml";

public var NumberOfXMLFiles:int=4;

		
		public function XMLHelpers(callingRepository:Object)
		{
			_callingRepository=callingRepository;
		}

		public function ReadEditorialXMLAsynch():void
		{
			var loader:URLLoader=new URLLoader();
			loader.dataFormat= URLLoaderDataFormat.TEXT;
			loader.addEventListener(Event.COMPLETE, ReadEditorialXMLAsynchComplete);
	var request:URLRequest = new URLRequest(_xMLEditorialFile);
          
                loader.load(request);
         	
		}
		
		
		
		public function ReadStillLifeXMLAsynch():void
		{
			var loader:URLLoader=new URLLoader();
			loader.dataFormat= URLLoaderDataFormat.TEXT;
			loader.addEventListener(Event.COMPLETE, ReadStillLifeXMLAsynchComplete);
	var request:URLRequest = new URLRequest(_xMLStillLifeFile);
          
                loader.load(request);
         	
		}
		
		
		
		
		
		public function ReadAdvertisingXMLAsynch():void
		{
			var loader:URLLoader=new URLLoader();
			loader.dataFormat= URLLoaderDataFormat.TEXT;
			loader.addEventListener(Event.COMPLETE, ReadAdvertisingXMLAsynchComplete);
	var request:URLRequest = new URLRequest(_xMLAdvertisingFile);
          
                loader.load(request);
         	
		}
		
		public function ReadRecentWorkXMLAsynch():void
		{
			var loader:URLLoader=new URLLoader();
			loader.dataFormat= URLLoaderDataFormat.TEXT;
			loader.addEventListener(Event.COMPLETE, ReadRecentWorkXMLAsynchComplete);
	var request:URLRequest = new URLRequest(_xMLRecentWorkFile);
          
                loader.load(request);
         	
		}
		
		
		public function ReadArchiveXMLAsynch():void
		{
			var loader:URLLoader=new URLLoader();
			loader.dataFormat= URLLoaderDataFormat.TEXT;
			loader.addEventListener(Event.COMPLETE, ReadArchiveXMLAsynchComplete);
	var request:URLRequest = new URLRequest(_xMLArchiveFile);
          
                loader.load(request);
         	
		}
		
//		public function ReadXMLSynch(xMLFile:String):XML
//		{
//			var file:File = File.documentsDirectory.resolvePath(xMLFile);
//			var fileStream:FileStream = new FileStream();
//			fileStream.open(file, FileMode.READ);
//			var prefsXML:XML = XML(fileStream.readUTFBytes(fileStream.bytesAvailable));
//			fileStream.close();
//
//		}
		

		public function WriteToXML():void
		{


		}

		private function ReadEditorialXMLAsynchComplete(event:Event):void
		{
			try
			{
			
				// Convert the downlaoded text into an XML instance
				var xMLData:XML=new XML(event.target.data);

				trace(xMLData);
				_callingRepository.SetXMLContents(xMLData, MenuItems.Editorial);
			}
			catch (e:TypeError)
			{
				// If we get here, that means the downloaded text could
				// not be converted into an XML instance, probably because
				// it is not formatted correctly.
				trace("Could not parse text into XML");
				trace(e.message);
			}
		}
		
		
		
		private function ReadStillLifeXMLAsynchComplete(event:Event):void
		{
			try
			{
				
				// Convert the downlaoded text into an XML instance
				var xMLData:XML=new XML(event.target.data);

				trace(xMLData);
				_callingRepository.SetXMLContents(xMLData, MenuItems.StillLifes);
			}
			catch (e:TypeError)
			{
				// If we get here, that means the downloaded text could
				// not be converted into an XML instance, probably because
				// it is not formatted correctly.
				trace("Could not parse text into XML");
				trace(e.message);
			}
		}
		
		
		private function ReadAdvertisingXMLAsynchComplete(event:Event):void
		{
			try
			{
				
				// Convert the downlaoded text into an XML instance
				var xMLData:XML=new XML(event.target.data);

				trace(xMLData);
				_callingRepository.SetXMLContents(xMLData, MenuItems.Advertising);
			}
			catch (e:TypeError)
			{
				// If we get here, that means the downloaded text could
				// not be converted into an XML instance, probably because
				// it is not formatted correctly.
				trace("Could not parse text into XML");
				trace(e.message);
			}
		}
		
		private function ReadRecentWorkXMLAsynchComplete(event:Event):void
		{
			try
			{
				
				// Convert the downlaoded text into an XML instance
				var xMLData:XML=new XML(event.target.data);

				trace(xMLData);
				_callingRepository.SetXMLContents(xMLData, MenuItems.RecentWork);
			}
			catch (e:TypeError)
			{
				// If we get here, that means the downloaded text could
				// not be converted into an XML instance, probably because
				// it is not formatted correctly.
				trace("Could not parse text into XML");
				trace(e.message);
			}
		}
		
			private function ReadArchiveXMLAsynchComplete(event:Event):void
		{
			try
			{
				
				// Convert the downlaoded text into an XML instance
				var xMLData:XML=new XML(event.target.data);

				trace(xMLData);
				_callingRepository.SetXMLContents(xMLData, MenuItems.Archive);
			}
			catch (e:TypeError)
			{
				// If we get here, that means the downloaded text could
				// not be converted into an XML instance, probably because
				// it is not formatted correctly.
				trace("Could not parse text into XML");
				trace(e.message);
			}
		}
	}
}