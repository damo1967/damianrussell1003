package Helpers
{

public final class ColorUtil
{

    /**
     * Converts an integer (hex) value to an object with separate r, g, 
     * and b elements.
     */
    public static function intToRgb( color:int ):Object
    {
        var r:int = ( color & 0xFF0000 ) >> 16;
        var g:int = ( color & 0x00FF00 ) >> 8;
        var b:int = color & 0x0000FF;
        return {r:r, g:g, b:b};
    }
    
} // end class
} // end package