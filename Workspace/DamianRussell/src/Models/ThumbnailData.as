package Models
{
	public class ThumbnailData
	{
		public var ImageName:String;
		public var Caption:String;
		public var Index:Number;
		public var Guid:String;
		
		public function ThumbnailData()
		{
		}

	}
}