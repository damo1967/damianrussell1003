package Models
{

import Helpers.XMLHelpers;

import flash.display.*;
import flash.events.*;
import flash.net.*;
import flash.utils.*;

	public class InteriorsRepository
	{
		private var _xMLFile:String = "../Databases/Interiors.xml";
		private var _xMLContents:XML;
		private var _xMLHelpers:XMLHelpers = new XMLHelpers(this);
		private var _interiorsController:Object;
		
		public function InteriorsRepository(interiorsController:Object)
		{
			_interiorsController = interiorsController;
		}

		public function ReadAllInteriors():void
		{
			//begin async read xml file
			_xMLHelpers.ReadXMLAsynch(_xMLFile);


		}
		
		public function XMLContents(xMLContents:XML):void
	{
		//receive asyn result
		_xMLContents = xMLContents;
		
		//send data back to controller
		_interiorsController.DataReceived(xMLContents);
		
	}
		
		

	}
}