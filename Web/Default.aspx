﻿<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	 <meta name="description" content="Damian Russell - Interiors and still life photographer. Represented by One Photographic. www.onephotographic.com." />
    <meta name="keywords" content="Damian Russell, London, photographer, Interiors, still, life, UK, art, photography, photo, onephotographic" />
    <meta name="author" content="http://www.damianrussell.com" />
    <meta name="copyright" content="Damian Russell&copy;2008" />
    <meta name="language" content="en" />
    <title>Damian Russell - Photographer - London - OnePhotographic - Still life - Interiors - Photography</title>
    <link rel="shortcut icon" href="Content/Images/favicon.ico" type="image/x-icon" />

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<!--  BEGIN Browser History required section -->
	<link rel="stylesheet" type="text/css" href="history/history.css" />
	<!--  END Browser History required section -->
	
	<script src="AC_OETags.js" language="javascript"></script>

	<!--  BEGIN Browser History required section -->

	<script src="history/history.js" language="javascript"></script>

	<!--  END Browser History required section -->
	<style>
		body
		{
			margin: 0px;
			overflow: hidden;
		}
	</style>

	<script language="JavaScript" type="text/javascript">
<!--
// -----------------------------------------------------------------------------
// Globals
// Major version of Flash required
var requiredMajorVersion = 9;
// Minor version of Flash required
var requiredMinorVersion = 0;
// Minor version of Flash required
var requiredRevision = 124;
// -----------------------------------------------------------------------------
// -->
	</script>

</head>
<body scroll="no">

	<script language="JavaScript" type="text/javascript">
<!--
// Version check for the Flash Player that has the ability to start Player Product Install (6.0r65)
var hasProductInstall = DetectFlashVer(6, 0, 65);

// Version check based upon the values defined in globals
var hasRequestedVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);

if ( hasProductInstall && !hasRequestedVersion ) {
	// DO NOT MODIFY THE FOLLOWING FOUR LINES
	// Location visited after installation is complete if installation is required
	var MMPlayerType = (isIE == true) ? "ActiveX" : "PlugIn";
	var MMredirectURL = window.location;
    document.title = document.title.slice(0, 47) + " - Flash Player Installation";
    var MMdoctitle = document.title;

	AC_FL_RunContent(
		"src", "playerProductInstall",
		"FlashVars", "MMredirectURL="+MMredirectURL+'&MMplayerType='+MMPlayerType+'&MMdoctitle='+MMdoctitle+"",
		"width", "100%",
		"height", "100%",
		"align", "middle",
		"id", "DamianRussell",
		"quality", "high",
		"bgcolor", "#000000",
		"name", "DamianRussell",
		"allowScriptAccess","sameDomain",
		"allowFullScreen","true",
		"type", "application/x-shockwave-flash",
		"pluginspage", "http://www.adobe.com/go/getflashplayer"
	);
} else if (hasRequestedVersion) {
	// if we've detected an acceptable version
	// embed the Flash Content SWF when all tests are passed
	AC_FL_RunContent(
			"src", "DamianRussell",
			"width", "100%",
			"height", "100%",
			"align", "middle",
			"id", "DamianRussell",
			"quality", "high",
			"bgcolor", "#000000",
			"name", "DamianRussell",
			"allowScriptAccess","sameDomain",
			"allowFullScreen","true",
			"type", "application/x-shockwave-flash",
			"pluginspage", "http://www.adobe.com/go/getflashplayer"
	);
  } else {  // flash is too old or we can't detect the plugin
    var alternateContent = '<p>This site requires the Adobe Flash Player. Please <a href=http://www.adobe.com/go/getflash/>download the Flash player</a></p>'
 
   	+ '<p><a href=Default.aspx#category=Menu;imageGuid=None>Menu</a></p>'
   	+ '<p><a href=Default.aspx#category=Editorial;imageGuid=None>Editorial</a></p>'
   	+ '<p><a href=Default.aspx#category=Still%20Life;imageGuid=None>Still Life</a></p>'
   	+ '<p><p><a href=Default.aspx#category=Advertising;imageGuid=None>Advertising</a></p>'
   	+ '<a href=Default.aspx#category=Complete;imageGuid=None>Comlete</a></p>'
   	+ '<p><a href=Default.aspx#category=Archive;imageGuid=None>Archive</a></p>'
   	 	+ '<p><a href=Default.aspx#category=Biography;imageGuid=None>Biography</a></p>'
   	 	 	+ '<p><a href=Default.aspx#category=Contacts;imageGuid=None>Contacts</a></p>'
   	+ '<p><a href=Default.aspx#category=Clients;imageGuid=None>Clients</a></p>';
    document.write(alternateContent);  // insert non-flash content
  }
// -->
	</script>

	<noscript>
		<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" id="DamianRussell" width="100%"
			height="100%" codebase="http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab">
			<param name="movie" value="DamianRussell.swf" />
			<param name="quality" value="high" />
			<param name="bgcolor" value="#000000" />
			<param name="allowScriptAccess" value="sameDomain" />
			<param name="allowFullScreen" value="true">
			<embed src="DamianRussell.swf" quality="high" bgcolor="#000000" width="100%" height="100%"
				name="DamianRussell" align="middle" play="true" loop="false" quality="high" allowscriptaccess="sameDomain"
				allowfullscreen="true" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer">
			</embed>
		</object>
	</noscript>
	

<%--	google analytics--%>

<script type="text/javascript">

	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-8346065-1']);
	_gaq.push(['_trackPageview']);

	(function () {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();

</script>

</body>
</html> 