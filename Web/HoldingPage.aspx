<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="description" content="Damian Russell - Interiors and still life photographer. Represented by One Photographic. www.onephotographic.com." />
<meta name="keywords" content="Damian Russell, London, photographer, Interiors, still, life, UK, art, photography, photo, onephotographic" />
<meta name="author" content="http://www.damianrussell.com" />
<meta name="copyright" content="Damian Russell&copy;2008" />
<meta name="language" content="en" />

<title>Damian Russell - Photography</title>
<link href="Styles/Shared.css" rel="stylesheet" type="text/css" media="all" />
<link rel="shortcut icon" href="Content/Images/favicon.ico" type="image/x-icon" />
</head>

<body style="background:#f5f0ea">
	<div id="wrap">
    	<div id="Logo" >
    	<img src="Content/Images/Holding-page.jpg" alt="Damian Russull Photographer" title="Damian Russull Photographer" border="0" usemap="#Map" />
<map name="Map" id="Map">
  <area shape="rect" coords="791,530,844,582" href="Admin/AdminMenu.aspx" />
<area shape="poly" coords="591,456,591,456,733,530,737,518,598,444" href="mailto:belinda@onephotographic.com" alt="send email" title="send email" />
</map> </div>
</div>

<%--Google analytics begin--%>

    <script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>

    <script type="text/javascript">
var pageTracker = _gat._getTracker("UA-8346065-1"); pageTracker._trackPageview();
    </script>

    <%--Google analytics end--%>
</body>
</html>
