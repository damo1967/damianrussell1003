﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class Admin_RestoreImagesDB : System.Web.UI.Page
{
    

    protected void Page_Load(object sender, EventArgs e)
    {
        string x = "";
    }

    private void WriteToFile(string strPath, ref byte[] Buffer)
    {
        // Create a file
        FileStream newFile = new FileStream(strPath, FileMode.Create);

        // Write data to the file
        newFile.Write(Buffer, 0, Buffer.Length);

        // Close file
        newFile.Close();

        ////write to xml file
        //WriteToXML(hfFileName.Value, hfCaption.Value);




    }
    
    protected void cvAdvertisingFileToUpload_ServerValidate(object source, ServerValidateEventArgs args)
    {
        string sFileName = args.Value;
        if (sFileName == "AdvertisingImages.xml")
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }

    protected void bAdvertisingFileToUpload_Click(object sender, EventArgs e)
    {
        UploadFile(AdvertisingFileToUpload);
    }

    protected void cvRecentWorkFileToUpload_ServerValidate(object source, ServerValidateEventArgs args)
    {
        string sFileName = args.Value;
        if (sFileName == "RecentWorkImages.xml")
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }

    protected void bRecentWorkFileToUpload_Click(object sender, EventArgs e)
    {
        UploadFile(RecentWorkFileToUpload);
    }

    protected void cvArchiveFileToUpload_ServerValidate(object source, ServerValidateEventArgs args)
    {
        string sFileName = args.Value;
        if (sFileName == "ArchiveImages.xml")
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }

    protected void bArchiveFileToUpload_Click(object sender, EventArgs e)
    {
        UploadFile(ArchiveFileToUpload);
    }

    protected void cvEditorialFileToUpload_ServerValidate(object source, ServerValidateEventArgs args)
    {
        string sFileName = args.Value;
        if (sFileName == "EditorialImages.xml")
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }

    protected void bEditorialFileToUpload_Click(object sender, EventArgs e)
    {
        UploadFile(EditorialFileToUpload);
    }

    protected void cvStillLifeFileToUpload_ServerValidate(object source, ServerValidateEventArgs args)
    {
        string sFileName = args.Value;
        if (sFileName == "StillLifeImages.xml")
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }

    protected void bStillLifeFileToUpload_Click(object sender, EventArgs e)
    {
        UploadFile(StillLifeFileToUpload);
    }

    private void UploadFile(FileUpload fileUpload)
    {
        // Check to see if file was uploaded and filename is correctly validated
        if ((fileUpload.PostedFile != null) && (Page.IsValid))
        {
            // Get a reference to PostedFile object
            HttpPostedFile myFile = fileUpload.PostedFile;

            // Get size of uploaded file
            int nFileLen = myFile.ContentLength;

            // make sure the size of the file is > 0
            if (nFileLen > 0)
            {
                // Allocate a buffer for reading of the file
                byte[] myData = new byte[nFileLen];

                // Read uploaded file from the Stream
                myFile.InputStream.Read(myData, 0, nFileLen);

                // Create a name for the file to store
                string strFilename = Path.GetFileName(myFile.FileName);

                // Write data into a file
                WriteToFile(Server.MapPath("../" + strFilename), ref myData);

                pStep1.Visible = false;
                pConfirmation.Visible = true;

            }


        }
        else //not validated 
        {

        }
    }
}
