﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Xml;
using System.Data;
using System.Configuration;

public partial class Web_Admin_AddImage : System.Web.UI.Page
{
    string _editorialXMLFilePath = ConfigurationManager.AppSettings["editorialXMLFilePath"];
    string _stillLifeXMLFilePath = ConfigurationManager.AppSettings["stillLifeXMLFilePath"];
    string _advertisingXMLFilePath = ConfigurationManager.AppSettings["advertisingXMLFilePath"];
    string _archiveXMLFilePath = ConfigurationManager.AppSettings["archiveXMLFilePath"];
    string _privateXMLFilePath = ConfigurationManager.AppSettings["privateXMLFilePath"];
    string _recentWorkXMLFilePath = ConfigurationManager.AppSettings["recentWorkXMLFilePath"];

    string _currentXMLFile = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CategoryDDL.Items.Add("Advertising");
            CategoryDDL.Items.Add("Archive");
            CategoryDDL.Items.Add("Editorial");
            CategoryDDL.Items.Add("Still Life");
            CategoryDDL.Items.Add("Recent Work");
            CategoryDDL.Items.Add("Private");
        }
    }




    private void WriteToFile(string strPath, ref byte[] Buffer)
    {
        // Create image file
      FileStream newFile = new FileStream(strPath, FileMode.Create);

        // Write data to the file
        newFile.Write(Buffer, 0, Buffer.Length);

        // Close file
        newFile.Close();


       
       


    }

    protected void UploadWizard_ActiveStepChanged(object sender, EventArgs e)
    {
        if (UploadWizard.ActiveStepIndex == 1)//if on preview confirmation page
        {

            //display details of upload
            lCaptionConfirm.Text = hfCaption.Value;
            iUploadedFile.ImageUrl = "../CMSImages/" + hfFileName.Value;
        }


    }



    protected void StartNextButton_Click(object sender, EventArgs e)
    {
        string sImageName = "";
        string sThumbnailName = "";

        // Check to see if file was uploaded
        if (FileToUpload.PostedFile != null)
        {
            // Get a reference to PostedFile object
            HttpPostedFile myImageFile = FileToUpload.PostedFile;
            string imageFilePath = ""; 


            // Get size of uploaded file
            int nImageFileLen = myImageFile.ContentLength;
         

            // make sure the size of the file is > 0
            if (nImageFileLen > 0)
            {
                // Allocate a buffer for reading of the file
                byte[] myData = new byte[nImageFileLen];

                // Read uploaded file from the Stream
                myImageFile.InputStream.Read(myData, 0, nImageFileLen);

                // Create a name for the file to store
                sImageName = Path.GetFileNameWithoutExtension(myImageFile.FileName) + "-" + Guid.NewGuid().ToString() + Path.GetExtension(myImageFile.FileName);
                imageFilePath = "../CMSImages/" + sImageName  ;

                
                // Write image data into a file
                
                WriteToFile(Server.MapPath(imageFilePath), ref myData);

                

            }

        //create the thumbnail
            

            // create an image object, using the filename we just retrieved
            System.Drawing.Image image = System.Drawing.Image.FromFile(Server.MapPath(imageFilePath));

            // create the actual thumbnail image
            System.Drawing.Image thumbnailImage = image.GetThumbnailImage(100, 66, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback), IntPtr.Zero);

            // make a memory stream to work with the image bytes
            //MemoryStream imageStream = new MemoryStream();

            FileStream thumbnailFileStream = new FileStream(Server.MapPath("../CMSImages/Thumbnails/" + sImageName), FileMode.Create);

            // put the image into the memory stream
            thumbnailImage.Save(thumbnailFileStream, System.Drawing.Imaging.ImageFormat.Jpeg);

            //// make byte array the same size as the image
            //byte[] imageContent = new Byte[imageStream.Length];

            //// rewind the memory stream
            //imageStream.Position = 0;

            //// load the byte array with the image
            //imageStream.Read(imageContent, 0, (int)imageStream.Length);

            //// return byte array to caller with image type
            //Response.ContentType = "image/jpeg";
            //Response.BinaryWrite(imageContent);



            //get file names and caption to use later
            hfFileName.Value = sImageName;
            hfCaption.Value = txtCaption.Text;

            //write to xml file
            WriteToXML(hfFileName.Value, hfCaption.Value);


        }
    }

    public bool ThumbnailCallback()
    {
        return true;
    }


    private void WriteToXML(string sImage, string sCaption)
        {
            switch (CategoryDDL.Text)
            {
                case "Editorial":
                    _currentXMLFile = _editorialXMLFilePath;
                    break;
                case "Still Life":
                    _currentXMLFile = _stillLifeXMLFilePath;
                    break;
                case "Advertising":
                    _currentXMLFile = _advertisingXMLFilePath;
                    break;
                case "Archive":
                    _currentXMLFile = _archiveXMLFilePath;
                    break;
                case "Private":
                    _currentXMLFile = _privateXMLFilePath;
                    break;
                case "Recent Work":
                    _currentXMLFile = _recentWorkXMLFilePath;
                    break;

            }

           // DataTable dt = new DataTable();


            XmlDataSource xdsImages = new XmlDataSource();
            xdsImages.DataFile = _currentXMLFile;
        
            XmlDocument xmldoc = xdsImages.GetXmlDocument();

            // New XML Element Created
            XmlElement XMLentry = xmldoc.CreateElement("pic");

            // First Element - image - Created
            XmlElement firstelement = xmldoc.CreateElement("imageName");

            // Value given for the first element
            firstelement.InnerText = sImage;

            // Append the newly created element as a child element
            XMLentry.AppendChild(firstelement);


            // Second Element - caption - Created
            XmlElement secondelement = xmldoc.CreateElement("caption");

            // Value given for the second element
            secondelement.InnerText = sCaption;

            // Append the newly created element as a child element
            XMLentry.AppendChild(secondelement);


            // Third Element - guid - Created
            XmlElement thirdelement = xmldoc.CreateElement("category");

            // Value given for the second element
            thirdelement.InnerText = CategoryDDL.SelectedValue;

            // Append the newly created element as a child element
            XMLentry.AppendChild(thirdelement);




            // Forth Element - guid - Created
            XmlElement forthElement = xmldoc.CreateElement("guid");

            // Value given for the second element
            forthElement.InnerText = Guid.NewGuid().ToString();

            // Append the newly created element as a child element
            XMLentry.AppendChild(forthElement);




            // New XML element inserted into the document
            xmldoc.DocumentElement.InsertAfter(XMLentry, xmldoc.DocumentElement.LastChild);

            xdsImages.Save();
        }



    protected void ibAdminMenu_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("AdminMenu.aspx");
    }
}
