﻿<%@ Page Language="C#" MasterPageFile="~/Masters/AdminMaster.master" AutoEventWireup="true"
	CodeFile="EditImages.aspx.cs" Inherits="Admin_EditImages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="Server">
    <div id="Div1" style="float: right">
		<p>
			<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="AdminMenu.aspx">Back to Admin Menu</asp:HyperLink>
		</p>
		<div>
		<p>
			<span class="BoldHeading" style="margin-right: 400px;">Edit Images</span> View 
			category:
		
			<asp:DropDownList ID="SortByCategoryDDL" runat="server" OnDataBound="CategoryDDL_OnLoad"
				OnSelectedIndexChanged="SortByCategoryDDL_SelectedIndexChanged" AutoPostBack="true">
				<asp:ListItem Selected="True">Editorial</asp:ListItem>
				<asp:ListItem>Still Life</asp:ListItem>
				
				<asp:ListItem>Advertising</asp:ListItem>
				<asp:ListItem>Archive</asp:ListItem>
				<asp:ListItem>Recent Work</asp:ListItem>
				<asp:ListItem>Private</asp:ListItem>
                
			</asp:DropDownList>
		</p>
		</div>
		<asp:GridView ID="gvImages" runat="server" AllowPaging="False" AllowSorting="False"
			AutoGenerateColumns="False" OnRowCommand="gvImages_RowCommand" OnRowDeleting="gvImages_RowDeleting"
			OnRowEditing="gvImages_RowEditing" OnRowUpdating="gvImages_RowUpdating" OnRowDataBound="gvImages_RowDataBound"
			OnRowCancelingEdit="gvImages_RowCancelingEdit" BackColor="White" BorderColor="#E7E7FF"
			BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" 
            OnInit="gvImages_Init" Width="900" 
            EmptyDataText="There are no images in this category at the moment">
			<%--        <Columns>
            <asp:CommandField ShowDeleteButton="True" />
            <asp:CommandField ShowEditButton="True" />
        </Columns>--%>
			<RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
			<Columns>
				<asp:TemplateField HeaderText="Complete Position">
					<ItemTemplate>
						<asp:Literal ID="litCurrentPosition" runat="server"></asp:Literal>
						<asp:HiddenField ID="hfImageGuid" runat="server" />
                        <asp:HiddenField ID="hfImageName" runat="server" />
 
					</ItemTemplate>
				</asp:TemplateField>
                	<%-- **********--%>
				<asp:TemplateField HeaderText="Move To Position">
					<ItemTemplate>
						<asp:Button runat="server" ID="bMoveToPosition" Text="Move To Position" CommandName="MoveToPosition"
							CommandArgument="<%#Container.DataItemIndex%>" />
						<asp:DropDownList ID="ddlMoveToPosition" runat="server">
						</asp:DropDownList>
					</ItemTemplate>
				</asp:TemplateField>
                	<%-- **********--%>
				<asp:TemplateField HeaderText="Move Up/Down">
					<ItemTemplate>
						<asp:Button runat="server" ID="bMoveUp" Text="Move Up" CommandName="MoveUp" CommandArgument="<%#Container.DataItemIndex%>" />
						<asp:Button runat="server" ID="bMoveDown" Text="Move Down" CommandName="MoveDown"
							CommandArgument="<%#Container.DataItemIndex%> " />
					</ItemTemplate>
				</asp:TemplateField>
				<%-- **********--%>
				<asp:TemplateField HeaderText="Current Category">
					<ItemTemplate>
						<asp:Label ID="Category" runat="server" Text='<%# Eval("category") %>'></asp:Label>
					</ItemTemplate>
					<EditItemTemplate>
						<asp:Label ID="Category" runat="server" Text='<%# Eval("category") %>'></asp:Label>
					</EditItemTemplate>
				</asp:TemplateField>
				<%-- **********--%>
          
			<asp:TemplateField HeaderText="Change Category">
					<ItemTemplate>
						<asp:Button runat="server" ID="bChangeCategory" Text="Change Category" CommandName="ChangeCategory"
							CommandArgument="<%#Container.DataItemIndex%>" />
						<asp:DropDownList ID="ddlChangeCategory" runat="server">
						</asp:DropDownList>
					</ItemTemplate>
				</asp:TemplateField>
                         	<%--*******--%>
				<asp:TemplateField HeaderText="Caption">
					<ItemTemplate>
						<asp:Label ID="caption" runat="server" Text='<%# Eval("caption") %>'></asp:Label>
					</ItemTemplate>
					<EditItemTemplate>
						<asp:TextBox runat="server" ID="Editcaption" Text='<%# Bind("caption") %>' />
					</EditItemTemplate>
				</asp:TemplateField>
				<%-- *********--%>
				<asp:TemplateField HeaderText="Thumbnail">
					<ItemTemplate>
						<asp:Image ID="Preview" runat="server" Width="80" Height="60" />
					</ItemTemplate>
					<EditItemTemplate>
						<asp:Image ID="Preview" runat="server" Width="80" Height="60" />
					</EditItemTemplate>
				</asp:TemplateField>
				<%--*******--%>
				<asp:TemplateField HeaderText="Commands">
					<ItemTemplate>
						<asp:Button runat="server" ID="Edit" Text="Edit" CommandName="Edit" />
						<asp:Button runat="server" ID="Delete" Text="Delete" CommandName="Delete" />
					</ItemTemplate>
					<EditItemTemplate>
						<asp:Button runat="server" ID="Update" Text="Update" CommandName="Update" />
						<asp:Button runat="server" ID="Cancel" Text="Cancel" CommandName="Cancel" />
					</EditItemTemplate>
				</asp:TemplateField>
             
			</Columns>
			<FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
			<PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
			<SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
			<HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
			<AlternatingRowStyle BackColor="#F7F7F7" />
		</asp:GridView>
	</div>
	<asp:XmlDataSource ID="xdsImages" runat="server" DataFile="~/Images.xml"></asp:XmlDataSource>
	<asp:HiddenField ID="hfImageName" runat="server" />
	<asp:HiddenField ID="hfCaption" runat="server" />
	<asp:HiddenField ID="hfCategory" runat="server" />
	<asp:HiddenField ID="hfGuid" runat="server" />
	<asp:HiddenField ID="hfSortCategory" runat="server" />
	<div id="menu">
		<br />
		<br />
		<asp:HyperLink ID="hlAdminMenu" runat="server" NavigateUrl="AdminMenu.aspx">Back to Admin Menu</asp:HyperLink>
	</div>
</asp:Content>
