﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;

public partial class Admin_BackupImagesDB : System.Web.UI.Page
{

    string _editorialXMLFilePath = ConfigurationManager.AppSettings["editorialXMLFilePath"];
    string _stillLifeXMLFilePath = ConfigurationManager.AppSettings["stillLifeXMLFilePath"];
    string _advertisingXMLFilePath = ConfigurationManager.AppSettings["advertisingXMLFilePath"];
    string _archiveXMLFilePath = ConfigurationManager.AppSettings["archiveXMLFilePath"];
    string _recentWorkXMLFilePath = ConfigurationManager.AppSettings["recentWorkXMLFilePath"];

    protected void Page_Load(object sender, EventArgs e)
    {

    }
  

    private void ReturnDownload(string xmlFilePath)
    {
        //this function is to ensure that the xml file downloads through the dialogue box and does not simply open up in the browser.
        string path = MapPath(xmlFilePath);
        string name = Path.GetFileName(path);
        string ext = Path.GetExtension(path);
        string type = "text/xml";
        Response.AppendHeader("content-disposition", "attachment; filename=" + name);
        Response.ContentType = type;
        Response.WriteFile(path);
        Response.End();
    }

    protected void ibAdminMenu_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("AdminMenu.aspx");
    }

    protected void bDownloadAdvertising_Click(object sender, EventArgs e)
    {
        ReturnDownload(_advertisingXMLFilePath);  
    }
    protected void bDownloadRecentWork_Click(object sender, EventArgs e)
    {
        ReturnDownload(_recentWorkXMLFilePath);
    }
    protected void bDownloadArchive_Click(object sender, EventArgs e)
    {
        ReturnDownload(_archiveXMLFilePath);  
    }
    protected void bDownloadEditorial_Click(object sender, EventArgs e)
    {
        ReturnDownload(_editorialXMLFilePath);  
    }
    protected void bDownloadStillLife_Click(object sender, EventArgs e)
    {
        ReturnDownload(_stillLifeXMLFilePath);  
    }
}
