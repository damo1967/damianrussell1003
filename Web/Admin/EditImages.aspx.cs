﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using System.Configuration;

public partial class Admin_EditImages : System.Web.UI.Page
{
    int _totalNumberOfRows = 0;
    DataSet _dsImages = new DataSet();
    string _currentSort = "";
    DataView _dataView;
    
    string _editorialXMLFilePath = ConfigurationManager.AppSettings["editorialXMLFilePath"];
    string _stillLifeXMLFilePath = ConfigurationManager.AppSettings["stillLifeXMLFilePath"];
    string _advertisingXMLFilePath = ConfigurationManager.AppSettings["advertisingXMLFilePath"];
    string _archiveXMLFilePath = ConfigurationManager.AppSettings["archiveXMLFilePath"];
    string _privateXMLFilePath = ConfigurationManager.AppSettings["privateXMLFilePath"];
    string _recentWorkXMLFilePath = ConfigurationManager.AppSettings["recentWorkXMLFilePath"];

    string _currentXMLFile = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        _currentSort = SortByCategoryDDL.Text;

           
            

            switch (_currentSort)
            {
                case "Editorial":
                    _currentXMLFile = _editorialXMLFilePath;
                    break;
                case "Still Life":
                    _currentXMLFile = _stillLifeXMLFilePath;
                    break;
                case "Advertising":
                    _currentXMLFile = _advertisingXMLFilePath;
                    break;
                case "Archive":
                    _currentXMLFile = _archiveXMLFilePath;
                    break;
                case "Private":
                    _currentXMLFile = _privateXMLFilePath;
                    break;
                case "Recent Work":
                    _currentXMLFile = _recentWorkXMLFilePath;
                    break;

            }

            //hfSortCategory.Value = SortByCategoryDDL.Text;

            RefreshImages();
            //if (_currentSort == "Editorial")
            //{
            //    _dataView.Sort = "category";
               
            //}
            //if (_currentSort == "complete")
            //{
            //    //_currentSort = "mixed";
            //}

   
            gvImages.DataSource = _dataView;


            //set value of sortDDL
            //SortByCategoryDDL.Text = _currentSort;

       
        if (!IsPostBack)
        {

            gvImages.DataBind();
   
 
        }


        //disable moveup and movedown buttons on first and last row of datagrid
        int liRows = gvImages.Rows.Count;
        if (liRows > 0)
        {
            Button bMoveUpFirst = (Button)gvImages.Rows[0].FindControl("bMoveUp");

            Button bMoveDownLast = (Button)gvImages.Rows[gvImages.Rows.Count - 1].FindControl("bMoveDown");
            bMoveUpFirst.Enabled = false;
            bMoveDownLast.Enabled = false;
        }

    }

    protected void gvImages_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvImages.EditIndex = e.NewEditIndex;
        //get original values
        GridViewRow row = gvImages.Rows[e.NewEditIndex];
            
        Image tempPreview = (Image)row.FindControl("Preview");
        Label tempCategory = (Label)row.FindControl("Category");
        Label tempcaption = (Label)row.FindControl("caption");
        HiddenField tempGuid = (HiddenField)row.FindControl("hfImageGuid");
        
        //DropDownList categoryDDL = (DropDownList)row.FindControl("CategoryDDL");
        //categoryDDL.SelectedValue = tempCategory.Text;

        hfImageName.Value = tempPreview.ImageUrl.Substring(tempPreview.ImageUrl.LastIndexOf("/")+1);
        hfCaption.Value = tempcaption.Text;
        hfCategory.Value = tempCategory.Text;
        hfGuid.Value = tempGuid.Value;

        RefreshImages();
        gvImages.DataBind();
    }

    protected void CategoryDDL_OnLoad(object sender, EventArgs e)
    {
        DropDownList tempCategoryDDL = (DropDownList)sender;
        tempCategoryDDL.SelectedValue = hfCategory.Value;
    }

    protected void gvImages_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        
        
        if (e.CommandName == "MoveUp")
        {
            int index = Convert.ToInt32(e.CommandArgument);

            // Retrieve the row that contains the button clicked 
            // by the user from the Rows collection.
            GridViewRow row = gvImages.Rows[index];
            Image tempPreview = (Image)row.FindControl("Preview");
           // string sOriginalimage = tempPreview.ImageUrl.Substring(tempPreview.ImageUrl.LastIndexOf("/") + 1);
            HiddenField tempGuid = (HiddenField)row.FindControl("hfImageGuid");
            int iNodeToMove = 0;
            //TextBox tempCaption = (TextBox)row.FindControl("EditCaption");
            //tDebug.Text = tempCategory.Text;

            //find in xml file
            XmlDocument xmlDocument = new System.Xml.XmlDocument();
            xmlDocument.PreserveWhitespace = true;

            xmlDocument.Load(Server.MapPath(_currentXMLFile));

            XmlNodeList elemList = xmlDocument.GetElementsByTagName("pic");
            for (int i = 0; i < elemList.Count; i++)
            {
                if (elemList[i].InnerXml.Contains(tempGuid.Value.ToString()))
                {
                    iNodeToMove = i;

                    break;
                }
            }

            
            XmlElement root = xmlDocument.DocumentElement;
           
            XmlNode nOrig = xmlDocument.GetElementsByTagName("pic")[iNodeToMove];
            XmlNode nSwap = xmlDocument.GetElementsByTagName("pic")[iNodeToMove-1];

            string sOrigValue = nOrig.InnerXml;

            nOrig.InnerXml = nSwap.InnerXml;
            nSwap.InnerXml = sOrigValue;

            xmlDocument.Save(Server.MapPath(_currentXMLFile));

            RefreshImages();
            
            gvImages.DataBind();
            //Response.Redirect("EditImages.aspx");

        }

        if (e.CommandName == "MoveDown")
        {
            int index = Convert.ToInt32(e.CommandArgument);

            // Retrieve the row that contains the button clicked 
            // by the user from the Rows collection.
            GridViewRow row = gvImages.Rows[index];
            Image tempPreview = (Image)row.FindControl("Preview");
            //string sOriginalimage = tempPreview.ImageUrl.Substring(tempPreview.ImageUrl.LastIndexOf("/") + 1);
            HiddenField tempGuid = (HiddenField)row.FindControl("hfImageGuid"); 
            int iNodeToMove = 0;


            //find in xml file
            XmlDocument xmlDocument = new System.Xml.XmlDocument();
            xmlDocument.PreserveWhitespace = true;

            xmlDocument.Load(Server.MapPath(_currentXMLFile));

            XmlNodeList elemList = xmlDocument.GetElementsByTagName("pic");
            for (int i = 0; i < elemList.Count; i++)
            {
                if (elemList[i].InnerXml.Contains(tempGuid.Value.ToString()))
                {
                    iNodeToMove = i;

                    break;
                }
            }


            XmlElement root = xmlDocument.DocumentElement;

            XmlNode nOrig = xmlDocument.GetElementsByTagName("pic")[iNodeToMove];
            XmlNode nSwap = xmlDocument.GetElementsByTagName("pic")[iNodeToMove + 1];

            string sOrigValue = nOrig.InnerXml;

            nOrig.InnerXml = nSwap.InnerXml;
            nSwap.InnerXml = sOrigValue;

            xmlDocument.Save(Server.MapPath(_currentXMLFile));
            RefreshImages();
            gvImages.DataBind();
            //Response.Redirect("EditImages.aspx");

        }

        if (e.CommandName == "MoveToPosition")
        {
            

            // Retrieve the row that contains the button clicked 
            // by the user from the Rows collection.
            int moveFrom = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = gvImages.Rows[moveFrom];

            //retrieve value of position to move to
            DropDownList ddlMoveTo = row.FindControl("ddlMoveToPosition") as DropDownList;
            int moveTo = int.Parse(ddlMoveTo.Text)-1;

            //find in xml file
            XmlDocument xmlDocument = new System.Xml.XmlDocument();
            xmlDocument.PreserveWhitespace = true;

            xmlDocument.Load(Server.MapPath(_currentXMLFile));

            XmlNodeList elemList = xmlDocument.GetElementsByTagName("pic");
            string nodeToMove = elemList[moveFrom].InnerXml;
            
            
            //if moving to higher position
            if (moveTo < moveFrom)
            {
                for (int i = moveFrom; i > moveTo; i--)
                {
                    elemList[i].InnerXml = elemList[i - 1].InnerXml;
                }

                elemList[moveTo].InnerXml = nodeToMove;

            }
            else //moving to a lower position
            {
                for (int i = moveFrom; i < moveTo; i++)
                {
                    elemList[i].InnerXml = elemList[i+1].InnerXml;
                }

                elemList[moveTo].InnerXml = nodeToMove;
            }


            xmlDocument.Save(Server.MapPath(_currentXMLFile));
         

            //move rows in dataview
            RefreshImages();

            gvImages.DataBind();
            //Response.Redirect("EditImages.aspx");
        }

        if (e.CommandName == "ChangeCategory")
        {


            // Retrieve the row that contains the button clicked 
            // by the user from the Rows collection.
            int rowId = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = gvImages.Rows[rowId];

            HiddenField guidHF =row.FindControl("hfImageGuid") as HiddenField;
            string guid = guidHF.Value.ToString();

            HiddenField hfImageName = row.FindControl("hfImageName") as HiddenField;
            string imageName = hfImageName.Value.ToString();

            Label captionLabel=row.FindControl("caption") as Label;
             string caption = captionLabel.Text;

            Label categoryLabel=row.FindControl("Category") as Label;
             string category = categoryLabel.Text;


            //retrieve value of position to move to
            DropDownList ddlChangeCategory = row.FindControl("ddlChangeCategory") as DropDownList;
            string newCategory = ddlChangeCategory.Text;
            string newCategoryXMLFile = "";
            switch (newCategory)
            {
                case "Editorial":
                    newCategoryXMLFile = _editorialXMLFilePath;
                    break;
                case "Still Life":
                    newCategoryXMLFile = _stillLifeXMLFilePath;
                    break;
                case "Advertising":
                    newCategoryXMLFile = _advertisingXMLFilePath;
                    break;
                case "Archive":
                    newCategoryXMLFile = _archiveXMLFilePath;
                    break;
                case "Private":
                    newCategoryXMLFile = _privateXMLFilePath;
                    break;
                case "Recent Work":
                    newCategoryXMLFile = _recentWorkXMLFilePath;
                    break;

            }

            //find in original xml file
            XmlDocument origninalXmlDocument = new System.Xml.XmlDocument();
            origninalXmlDocument.PreserveWhitespace = true;

            origninalXmlDocument.Load(Server.MapPath(_currentXMLFile));

            XmlNodeList elemList = origninalXmlDocument.GetElementsByTagName("pic");
            XmlNode nodeToMove = elemList[rowId];
            //string nodeToMove = elemList[rowId].InnerXml;

      
  





            //open new xml file
            XmlDocument newXmlDocument = new System.Xml.XmlDocument();
            newXmlDocument.PreserveWhitespace = true;

            newXmlDocument.Load(Server.MapPath(newCategoryXMLFile));


            //insert value into new category xms
            //  XmlDataSource xdsNewCategory = new XmlDataSource();
            //xdsNewCategory.DataFile = newCategoryXMLFile;
        
            //XmlDocument newXmlDocument = xdsNewCategory.GetXmlDocument();

            // New XML Element Created
            XmlElement XMLentry = newXmlDocument.CreateElement("pic");

            // First Element - image - Created
            XmlElement firstelement = newXmlDocument.CreateElement("imageName");

            // Value given for the first element
            firstelement.InnerText = imageName;

            // Append the newly created element as a child element
            XMLentry.AppendChild(firstelement);


            // Second Element - caption - Created
            XmlElement secondelement = newXmlDocument.CreateElement("caption");

            // Value given for the second element
            secondelement.InnerText = caption;

            // Append the newly created element as a child element
            XMLentry.AppendChild(secondelement);


            // Third Element - guid - Created
            XmlElement thirdelement = newXmlDocument.CreateElement("category");

            // Value given for the second element
            thirdelement.InnerText = newCategory;

            // Append the newly created element as a child element
            XMLentry.AppendChild(thirdelement);




            // Forth Element - guid - Created
            XmlElement forthElement = newXmlDocument.CreateElement("guid");

            // Value given for the second element
            forthElement.InnerText = guid;

             //Append the newly created element as a child element
            XMLentry.AppendChild(forthElement);




            // New XML element inserted into the document
            newXmlDocument.DocumentElement.InsertBefore(XMLentry, newXmlDocument.DocumentElement.FirstChild);
          //  newXmlDocument.DocumentElement.AppendChild(nodeToMove);
            newXmlDocument.Save(Server.MapPath(newCategoryXMLFile));


            //***************

            //remove from current xml 
         XmlNodeList allElements = origninalXmlDocument.GetElementsByTagName("pic");
         int iNodeToDelete = 0;
         for (int i = 0; i < allElements.Count; i++)
        {
            if (allElements[i].InnerXml.Contains(guid))
           {
               iNodeToDelete = i;
               
               break;
           }
        }
       
        XmlElement root = origninalXmlDocument.DocumentElement;
        XmlNode n = origninalXmlDocument.GetElementsByTagName("pic")[iNodeToDelete];
        root.RemoveChild(n);

        origninalXmlDocument.Save(Server.MapPath(_currentXMLFile));

                     

            //move rows in dataview
            RefreshImages();

            gvImages.DataBind();
            //Response.Redirect("EditImages.aspx");
        }
    }

    protected void gvImages_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //get original values
        GridViewRow row = gvImages.Rows[e.RowIndex];
        Image tempPreview = (Image)row.FindControl("Preview");
        string sOriginalimage = tempPreview.ImageUrl.Substring(tempPreview.ImageUrl.LastIndexOf("/")+1);
   
        int iNodeToDelete = 0;
        

        //find in xml file and remove
        XmlDocument xmlDocument = new System.Xml.XmlDocument();
        xmlDocument.PreserveWhitespace = true;

        xmlDocument.Load(Server.MapPath(_currentXMLFile));

        XmlNodeList elemList = xmlDocument.GetElementsByTagName("pic");
        for (int i = 0; i < elemList.Count; i++)
        {
           if (elemList[i].InnerXml.Contains(sOriginalimage))
           {
               iNodeToDelete = i;
               
               break;
           }
        }
       
        XmlElement root = xmlDocument.DocumentElement;
        XmlNode n = xmlDocument.GetElementsByTagName("pic")[iNodeToDelete];
        root.RemoveChild(n);

        xmlDocument.Save(Server.MapPath(_currentXMLFile));

        //delete files from hard disk
        //try
        //{
        FileInfo TheFile = new FileInfo(Server.MapPath("../CMSImages/" + sOriginalimage));
            if (TheFile.Exists)
            {
                File.Delete(Server.MapPath("../CMSImages/" + sOriginalimage));
                File.Delete(Server.MapPath("../CMSImages/Thumbnails/" + sOriginalimage));
            }
            else
            {
                throw new FileNotFoundException();
            }
        //}

        //catch 
        //{
            
        //}


        //amend the rows in the dataview
            _dataView.Table.Rows.RemoveAt(row.DataItemIndex);
        gvImages.DataBind();
        //Response.Redirect("EditImages.aspx");
        
    }

    protected void gvImages_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow row = gvImages.Rows[e.RowIndex];

        HiddenField guidHF = row.FindControl("hfImageGuid") as HiddenField;
        string sOriginalGuid = guidHF.Value.ToString();

        //Get old values from hidden fields
        String sOriginalImageName = Server.HtmlEncode(hfImageName.Value);
    //    string sOriginalGuid = hfGuid.Value;
   
        //get new values
        
        TextBox tempCaption = (TextBox)row.FindControl("EditCaption");
   
        
        String sNewCaption = Server.HtmlEncode(tempCaption.Text);
        Label tempCategory = (Label)row.FindControl("Category");
       

        string sNewCategory = tempCategory.Text;

        XElement root = XElement.Load(Server.MapPath(_currentXMLFile));
        XElement pic =
            (from el in root.Elements("pic")
            where (string)el.Element("guid") == sOriginalGuid
            select el).Single();

        pic.Element("caption").Value = sNewCaption;
        pic.Element("category").Value = sNewCategory;

        root.Save(Server.MapPath(_currentXMLFile));




        ////update the xml file
        //XmlDocument xmlDocument = new System.Xml.XmlDocument();
        //xmlDocument.PreserveWhitespace = true;

        //xmlDocument.Load(Server.MapPath(_currentXMLFile));
        //XmlNodeList allPicNodes = xmlDocument.GetElementsByTagName("pic");
        //foreach (XmlNode picNode in allPicNodes)
        //{
        //    string imageName = "";
        //    string caption = "";
        //    string category = "";
        //    string guid = "";

        //    foreach (XmlNode childNode in picNode)
        //    {
        //        if (childNode.Name == "imageName") imageName = childNode.Value;
        //        if (childNode.Name == "caption") caption = childNode.Value;
        //        if (childNode.Name == "category") category = childNode.Value;
        //        if (childNode.Name == "guid") guid = childNode.Value;
        //    }   

        //    if (imageName == sOriginalimage)
        //        picNode.InnerXml = "\r\n     <imageName>" + sOriginalimage + "</imageName>\r\n    <caption>" + sNewCaption + "</caption>\r\n   <category>" + sNewCategory + "</category>\r\n  <guid>" + guid + "</guid>\r\n";
        //       break;
        //}
        //for (int i = 0; i < allPicNodes.Count; i++)
        //{
        //   if (allPicNodes[i].InnerXml.Contains(sOriginalimage))
        //   {
        //       
        //   }
        //}

        //update xml file
       // xmlDocument.Save(Server.MapPath(_currentXMLFile));

        gvImages.EditIndex = -1;
        RefreshImages();
        gvImages.DataBind();

       // Response.Redirect("EditImages.aspx");
    }

    protected void gvImages_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //get current position
            Literal lPosition = (Literal)e.Row.FindControl("litCurrentPosition");
            lPosition.Text = DataBinder.Eval(e.Row.DataItem, "Position").ToString();

            //get the guid for the image
            HiddenField hfImageGuid = (HiddenField)e.Row.FindControl("hfImageGuid");
            hfImageGuid.Value = DataBinder.Eval(e.Row.DataItem, "guid").ToString();


            //get the imageName for the image
            HiddenField hfImageName = (HiddenField)e.Row.FindControl("hfImageName");
            hfImageName.Value = DataBinder.Eval(e.Row.DataItem, "imageName").ToString();

            //set the values of the moveto dropdown list
            DropDownList dMoveToPosition = (DropDownList)e.Row.FindControl("ddlMoveToPosition");
            for (int i = 1; i <= _totalNumberOfRows; i++)
            
            {
                int position = int.Parse(DataBinder.Eval(e.Row.DataItem, "Position").ToString());
                if (i != position)
                {
                    dMoveToPosition.Items.Add(i.ToString());
                }
            }

            //set the values of the change category dropdown list
            DropDownList ddlChangeCategory = (DropDownList)e.Row.FindControl("ddlChangeCategory");
            string nextCategory = ddlChangeCategory.Text;
            string currentCategory = DataBinder.Eval(e.Row.DataItem, "category").ToString();

            string[] allCategories = new string[6] { "Editorial", "Still Life", "Advertising", "Archive", "Recent Work", "Private" };
               
            foreach (string category in allCategories)
            {
                if (category != currentCategory)
                {
                    ddlChangeCategory.Items.Add(category);
                }
                
            }


            //hide move up and move down for top and bottom rows
            if (DataBinder.Eval(e.Row.DataItem, "Position").ToString() == "1")
            {
                Button moveUpButton = (Button)e.Row.FindControl("bMoveUp");
                moveUpButton.Enabled = false;
            }
            if (DataBinder.Eval(e.Row.DataItem, "Position").ToString() == _totalNumberOfRows.ToString())
            {
                Button moveUpButton = (Button)e.Row.FindControl("bMoveDown");
                moveUpButton.Enabled = false;
            }
            
            //attach javascript confirmation to delete buttons
            Button bDelete = (Button)e.Row.FindControl("Delete");
            string sConfirmation = "javascript:return confirm('Are you sure you want to delete " + DataBinder.Eval(e.Row.DataItem, "imageName") + "?');";
            if (bDelete != null)
            {
                bDelete.Attributes.Add("onclick", sConfirmation);
            }

            //set image sources for previews
            Image iPreview = (Image)e.Row.FindControl("Preview");
            string sPreviewPath = "../CMSImages/Thumbnails/" + DataBinder.Eval(e.Row.DataItem, "imageName");
            if (iPreview != null)
            {
                iPreview.Attributes.Add("Src", sPreviewPath);
                iPreview.ImageUrl = sPreviewPath;
            }

            
            

 
        }


        

        

    }
    protected void gvImages_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        //Response.Redirect("EditImages.aspx");
        gvImages.EditIndex = -1;
        gvImages.DataBind();
    }

    protected void ibAdminMenu_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("AdminMenu.aspx");
    }


    protected void gvImages_Init(object sender, EventArgs e)
    {
        //RefreshImages();
     
    }

    private void RefreshImages()
    {
        _dsImages.Clear();
        _dsImages.ReadXml(MapPath(_currentXMLFile));
        if (_dsImages.Tables.Count > 0)
        {
            if (!_dsImages.Tables[0].Columns.Contains("Position"))
            {
                _dsImages.Tables[0].Columns.Add("Position");
            }
            int counter = 1;
            foreach (DataRow row in _dsImages.Tables[0].Rows)
            {
                row["Position"] = counter;
                counter++;
            }
            _totalNumberOfRows = _dsImages.Tables[0].Rows.Count;

            _dataView = _dsImages.Tables[0].DefaultView;
        }
    }






    protected void SortByCategoryDDL_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        gvImages.DataBind();
            
        
    }

}
