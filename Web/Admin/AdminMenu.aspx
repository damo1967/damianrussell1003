﻿<%@ Page Language="C#" MasterPageFile="~/Masters/AdminMaster.master" AutoEventWireup="true" CodeFile="AdminMenu.aspx.cs" Inherits="Admin_AdminMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" Runat="Server">
    <table>
                <tr>
                    <td style="width: 350px;" align="center" valign="top">
                        &nbsp;<br />
                        <table border="1" bordercolor="#666666" class="Menus">
                            <tr>
                                <td style="width: 300px">
                                    <table style="width: 300px">
                                        <tr>
                                            <td align="left" style="width: 300px">
                                                                    Manage Images</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 300px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 300px; height: 18px;" align="left">
                                                <table>
                                                    <tr>
                                                        <td style="width: 35px">
                                                            <asp:ImageButton ID="ImageButton9" runat="server" 
                                                                ImageUrl="~/Content/Images/Add_Small.jpg" PostBackUrl="~/Admin/AddImage.aspx" /></td>
                                                        <td style="width: 265px">
        <asp:HyperLink ID="HyperLink2" runat="server" Font-Names="Verdana" NavigateUrl="~/Admin/AddImage.aspx" Font-Size="10pt" 
                                                                Width="157px">Add a New Image</asp:HyperLink></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 300px" align="left">
                                                <table>
                                                    <tr>
                                                        <td style="width: 35px">
                                                            <asp:ImageButton ID="ImageButton10" runat="server" 
                                                                ImageUrl="~/Content/Images/Edit_Small.jpg" 
                                                                PostBackUrl="~/Admin/EditImages.aspx" /></td>
                                                        <td style="width: 265px">
        
        <asp:HyperLink ID="HyperLink10" runat="server" Font-Names="Verdana" NavigateUrl="~/Admin/EditImages.aspx" Font-Size="10pt" 
                                                                Width="131px">Edit Images</asp:HyperLink></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br />
                            <table border="1" bordercolor="#666666" class="Menus">
                                <tr>
                                    <td style="width: 300px">
                                        <table style="width: 300px">
                                            <tr>
                                                <td align="left" style="width: 300px">
                                                    The Gallery</td>
                                            </tr>
                                            <tr>
                                                <td style="width: 300px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 300px; height: 18px;" align="left">
                                                    <table>
                                                        <tr>
                                                            <td style="width: 35px">
                                                                <asp:ImageButton ID="ImageButton4" runat="server" 
                                                                    ImageUrl="~/Content/Images/View_Small.jpg" PostBackUrl="~/default.aspx" /></td>
                                                            <td style="width: 265px">
                                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/default.aspx">View the Gallery</asp:HyperLink></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td align="left" style="width: 300px">
                                                    
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                    </td>
                    <td align="center" valign="top" style="width: 350px">
                        &nbsp;<table border="1" bordercolor="#666666" class="Menus">
                                <tr>
                                    <td style="width: 300px">
                                        <table style="width: 300px">
                                            <tr>
                                                <td align="left" style="width: 300px">
                    Manage Database</td>
                                            </tr>
                                            <tr>
                                                <td style="width: 300px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 300px; height: 18px;" align="left">
                                                    <table>
                                                        <tr>
                                                            <td style="width: 35px">
                                                                <asp:ImageButton ID="ImageButton7" runat="server" 
                                                                    ImageUrl="~/Content/Images/DeleteIncidentsUpdates_Small.jpg" 
                                                                    PostBackUrl="~/Admin/BackupImagesDB.aspx" /></td>
                                                            <td style="width: 265px">
                                                                <asp:HyperLink ID="HyperLink8" runat="server" Font-Names="Verdana" 
                                                                    Font-Size="10pt" Width="207px" NavigateUrl="~/Admin/BackupImagesDB.aspx">Make a Backup of the Images Database</asp:HyperLink></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 300px" align="left">
                                                    <table>
                                                        <tr>
                                                            <td style="width: 35px">
                                                                <asp:ImageButton ID="ImageButton8" runat="server" 
                                                                    ImageUrl="~/Content/Images/ProductPointsOfFailure_Small.jpg" 
                                                                    PostBackUrl="~/Admin/RestoreImagesDB.aspx" /></td>
                                                            <td style="width: 265px">
        <asp:HyperLink ID="HyperLink3" runat="server" Font-Names="Verdana" Font-Size="10pt" Width="245px" 
                                                                    NavigateUrl="~/Admin/RestoreImagesDB.aspx">Upload Previous Backup of Images Database</asp:HyperLink></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        
                                    </td>
                                </tr>
                            </table>
                        <br />
                        <table border="1" bordercolor="#666666" class="Menus">
                                <tr>
                                    <td style="width: 300px">
                                        <table style="width: 300px">
                                            <tr>
                                                <td align="left" style="width: 300px">
                                                    My Personal Settings</td>
                                            </tr>
                                            <tr>
                                                <td style="width: 300px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 300px; height: 18px;" align="left">
                                                    <table>
                                                        <tr>
                                                            <td style="width: 35px">
                                                                <asp:ImageButton ID="ImageButton2" runat="server" 
                                                                    ImageUrl="~/Content/Images/ChangePassword_Small.jpg" 
                                                                    PostBackUrl="~/Admin/UpdatePassword.aspx" /></td>
                                                            <td style="width: 265px">
                                                <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/Admin/UpdatePassword.aspx">Update My Password</asp:HyperLink></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 300px">
                                                    </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
               
             
                    </td>
                </tr>
            </table>
    
        <table>
           
        </table>

</asp:Content>

