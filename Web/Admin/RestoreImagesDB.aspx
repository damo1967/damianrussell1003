﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/AdminMaster.master" AutoEventWireup="true" CodeFile="RestoreImagesDB.aspx.cs" Inherits="Admin_RestoreImagesDB" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Main" Runat="Server">
      <asp:Panel ID="pConfirmation" runat="server" Width="300" Visible="false">
    <p style="color:Red;">
        <b>Confirmation</b><br />
        <br />
        The database was successfully uploaded.
  
</p>
    </asp:Panel>


   
            
    <asp:Panel ID="pStep1" runat="server" Width="500">
    
     <h2>Upload and restore a previous backup of the images database</h2>
  
      Use the buttons below to upload the database, one at a time, then wait for confirmation that 
            the upload was successful.<br />

            <span style="color:Red">Note: This will overwrite the current database, make a 
            backup of the current database if youre not sure.</span><br />
            <br />

        <%--   ***********--%>
            <h2>Upload Advertising database</h2>
          
            <span style="color:Red">(Hint) The database must be called "AdvertisingImages.xml".</span><br />
            <br />
            <asp:FileUpload ID="AdvertisingFileToUpload" runat="server" EnableTheming="False" /> 
            <asp:Button ID="bAdvertisingFileToUpload" runat="server" 
                Text="Upload Advertising Database" 
                onclick="bAdvertisingFileToUpload_Click" ValidationGroup="Advertising" />  
            <br />
            <asp:RequiredFieldValidator ID="valAdvertisingFileToUpload" runat="server" 
                ErrorMessage="A file name is required" ControlToValidate="AdvertisingFileToUpload" 
                Display="Dynamic" ValidationGroup="Advertising"></asp:RequiredFieldValidator>
            <asp:CustomValidator ID="cvAdvertisingFileToUpload" runat="server" 
                ControlToValidate="AdvertisingFileToUpload" ErrorMessage="The file name must be 'AdvertisingImages.xml'" 
      Display="Dynamic" onservervalidate="cvAdvertisingFileToUpload_ServerValidate" ValidationGroup="Advertising"></asp:CustomValidator>
       
                     <br />
         
      <%--   ***********--%>
            <h2>Upload Archive database</h2>
          
            <span style="color:Red">(Hint) The database must be called "ArchiveImages.xml".</span><br />
            <br />
            <asp:FileUpload ID="ArchiveFileToUpload" runat="server" EnableTheming="False" /> 
            <asp:Button ID="bArchiveFileToUpload" runat="server" 
                Text="Upload Archive Database" 
                onclick="bArchiveFileToUpload_Click" ValidationGroup="Archive" />  
            <br />
            <asp:RequiredFieldValidator ID="valArchiveFileToUpload" runat="server" 
                ErrorMessage="A file name is required" ControlToValidate="ArchiveFileToUpload" 
                Display="Dynamic" ValidationGroup="Archive"></asp:RequiredFieldValidator>
            <asp:CustomValidator ID="cvArchiveFileToUpload" runat="server" 
                ControlToValidate="ArchiveFileToUpload" ErrorMessage="The file name must be 'ArchiveImages.xml'" 
      Display="Dynamic" onservervalidate="cvArchiveFileToUpload_ServerValidate" ValidationGroup="Archive"></asp:CustomValidator>
       
                     <br />

                      <%--   ***********--%>
            <h2>Upload Editorial database</h2>
          
            <span style="color:Red">(Hint) The database must be called "EditorialImages.xml".</span><br />
            <br />
            <asp:FileUpload ID="EditorialFileToUpload" runat="server" EnableTheming="False" /> 
            <asp:Button ID="bEditorialFileToUpload" runat="server" 
                Text="Upload Editorial Database" 
                onclick="bEditorialFileToUpload_Click" ValidationGroup="Editorial" />  
            <br />
            <asp:RequiredFieldValidator ID="valEditorialFileToUpload" runat="server" 
                ErrorMessage="A file name is required" ControlToValidate="EditorialFileToUpload" 
                Display="Dynamic" ></asp:RequiredFieldValidator>
            <asp:CustomValidator ID="cvEditorialFileToUpload" runat="server" 
                ControlToValidate="EditorialFileToUpload" ErrorMessage="The file name must be 'EditorialImages.xml'" 
      Display="Dynamic" onservervalidate="cvEditorialFileToUpload_ServerValidate" ValidationGroup="Editorial"></asp:CustomValidator>
       
                     <br />

                               <%--   ***********--%>
            <h2>Upload Still Life database</h2>
          
            <span style="color:Red">(Hint) The database must be called "StillLifeImages.xml".</span><br />
            <br />
            <asp:FileUpload ID="StillLifeFileToUpload" runat="server" EnableTheming="False" /> 
            <asp:Button ID="bStillLifeFileToUpload" runat="server" 
                Text="Upload Still Life Database" 
                onclick="bStillLifeFileToUpload_Click" ValidationGroup="StillLife" />  
            <br />
            <asp:RequiredFieldValidator ID="valStillLifeFileToUpload" runat="server" 
                ErrorMessage="A file name is required" ControlToValidate="StillLifeFileToUpload" 
                Display="Dynamic" ValidationGroup="StillLife" ></asp:RequiredFieldValidator>
            <asp:CustomValidator ID="cvStillLifeFileToUpload" runat="server" 
                ControlToValidate="StillLifeFileToUpload" ErrorMessage="The file name must be 'StillLifeImages.xml'" 
      Display="Dynamic" onservervalidate="cvStillLifeFileToUpload_ServerValidate" 
                ValidationGroup="StillLife"></asp:CustomValidator>
       
                     <br />

					         <%--   ***********--%>
            <h2>Upload Recent Work database</h2>
          
            <span style="color:Red">(Hint) The database must be called "RecentWorkImages.xml".</span><br />
            <br />
            <asp:FileUpload ID="RecentWorkFileToUpload" runat="server" EnableTheming="False" /> 
            <asp:Button ID="bRecentWorkFileToUpload" runat="server" 
                Text="Upload RecentWork Database" 
                onclick="bRecentWorkFileToUpload_Click" ValidationGroup="RecentWork" />  
            <br />
            <asp:RequiredFieldValidator ID="valRecentWorkFileToUpload" runat="server" 
                ErrorMessage="A file name is required" ControlToValidate="RecentWorkFileToUpload" 
                Display="Dynamic" ValidationGroup="RecentWork"></asp:RequiredFieldValidator>
            <asp:CustomValidator ID="cvRecentWorkFileToUpload" runat="server" 
                ControlToValidate="RecentWorkFileToUpload" ErrorMessage="The file name must be 'RecentWorkImages.xml'" 
      Display="Dynamic" onservervalidate="cvRecentWorkFileToUpload_ServerValidate" ValidationGroup="RecentWork"></asp:CustomValidator>
       
                     <br />
    </asp:Panel>
    

    <br />
     <div id="menu" >
           <br /><br /><br />
        <asp:HyperLink ID="hlAdminMenu" runat="server" 
            NavigateUrl="AdminMenu.aspx">Back to Admin Menu</asp:HyperLink>
    </div>

</asp:Content>

