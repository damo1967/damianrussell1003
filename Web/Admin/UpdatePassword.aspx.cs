﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class Admin_UpdatePassword : System.Web.UI.Page
{
    MembershipUser muUser;

    protected void Page_Load(object sender, EventArgs e)
    {
        muUser = Membership.GetUser(User.Identity.Name);
        string stUserName = User.Identity.Name;
    }

    protected void butContinue_Click(object sender, EventArgs e)
    {
        Response.Redirect("AdminMenu.aspx");
    }

    protected void butCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("AdminMenu.aspx");
    }



    protected void butUpdatePassword_Click(object sender, EventArgs e)
    {
        lblPasswordError.Visible = false;

        try
        {
            bool blUpdatePassword = muUser.ChangePassword(tbOldPassword.Text, tbNewPassword.Text);
            if (blUpdatePassword == false)
            {
                lblPasswordError.Text = "The old password you provided was incorrect, please try again.";
                lblPasswordError.Visible = true;
                return;
            }
            pPassword.Visible = false;
            pPassword.Enabled = false;
            pSuccessful.Visible = true;
            pSuccessful.Enabled = true;
        }
        catch
        {
            lblPasswordError.Text = "There was an error updating your password details. Try again. If the error persists this may indicate a problem with the database.";
        }
    }

    protected void ibAdminMenu_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("AdminMenu.aspx");
    }
}
