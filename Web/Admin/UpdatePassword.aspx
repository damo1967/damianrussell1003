﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/AdminMaster.master" AutoEventWireup="true" CodeFile="UpdatePassword.aspx.cs" Inherits="Admin_UpdatePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" Runat="Server">

    
        

    <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                
                <td align="left" style="width: 595px" valign="top">
        <asp:Panel ID="pPassword" runat="server" Height="300px" Width="600px">
            <h2 style="text-align: left">
            Update your password :</h2><br />
            <table style="width: 600px">
                <tr>
                <td style="width: 150px; height: 26px;" align="left">
                    <strong>
                    Old Password:</strong></td>
                <td style="width: 150px; height: 26px;">
                    <asp:TextBox ID="tbOldPassword" runat="server" TextMode="Password" Width="145px"></asp:TextBox>
                    </td>
                <td style="width: 300px; color: #000000; height: 26px;" align="left">
                    <asp:RequiredFieldValidator ID="rfvOldPassword" runat="server" ControlToValidate="tbOldPassword"
                        ErrorMessage="Old Password Required" Width="265px"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                <td align="left">
                    <strong>New Password:</strong></td>
                <td>
                    <asp:TextBox ID="tbNewPassword" runat="server" TextMode="Password" Width="145px"></asp:TextBox>
                    </td>
                <td style="width: 100px; color: #000000;" align="left">
                    <asp:RequiredFieldValidator ID="rfvNewPassword" runat="server" ControlToValidate="tbNewPassword"
                        ErrorMessage="New Password Required" Width="266px"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                <td align="left">
                    <strong>
                    Confirm New Password:</strong></td>
                <td>
                    <asp:TextBox ID="tbConfirmNewPassword" runat="server" TextMode="Password" Width="145px"></asp:TextBox>
                    </td>
                <td style="width: 100px" align="left">
                    <asp:RequiredFieldValidator ID="rfvSecurityQuestion" runat="server" ControlToValidate="tbNewPassword"
                        ErrorMessage="Confirm New Password Required" Width="269px"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="tbConfirmNewPassword"
                        ControlToValidate="tbNewPassword" ErrorMessage="New Password and Confirm New Password must match" Width="432px"></asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="Button3" runat="server" OnClick="butCancel_Click" Text="Cancel" CausesValidation="False" /></td>
                    <td align="left">
                        <asp:Button ID="butUpdatePassword" runat="server" Text="Update Password" OnClick="butUpdatePassword_Click" /></td>
                    <td style="width: 100px">
                        </td>
                </tr>
                <tr>
                    <td colspan="3" align="left">
                        <asp:Label ID="lblPasswordError" runat="server" ForeColor="Red" Text="Label" Visible="False"></asp:Label></td>
                </tr>
            </table>
            <br />
            
            
        
            
        </asp:Panel>
        
      
        <asp:Panel ID="pSuccessful" runat="server" Height="200px" Visible="False" Width="600px" 
                        Enabled="False">
            <h2>Your password was successfully updated</h2>
            <br />
           
            <br />
  
            </asp:Panel>
                </td>
            </tr>
        </table>
        
    <div style = "float:left; position:relative;top:-70px;">

        <asp:HyperLink ID="hlAdminMenu" runat="server" 
            NavigateUrl="AdminMenu.aspx">Back to Admin Menu</asp:HyperLink>
    </div>
    
</asp:Content>

