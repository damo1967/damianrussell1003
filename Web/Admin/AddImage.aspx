﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Masters/AdminMaster.master" CodeFile="AddImage.aspx.cs" Inherits="Web_Admin_AddImage" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Main" Runat="Server">



    <h2>Add a new image:</h2>

    Step1: Upload image<br />
    Step2: Choose a category<br />
    Step3: Add a caption<br />
    Step4: Wait for confirmation<br />
    <br />
    <div>
    
        
        
        <%--**********wizard*********--%>
        
        
        <asp:Wizard ID="UploadWizard" runat="server" 
            onactivestepchanged="UploadWizard_ActiveStepChanged" 
            ActiveStepIndex="0" DisplaySideBar="False" style="margin-right: 0px" 
            FinishDestinationPageUrl="AddImage.aspx">
            <SideBarTemplate>
                <asp:DataList ID="SideBarList" runat="server">
                    <ItemTemplate>
                        <asp:LinkButton ID="SideBarButton" runat="server"></asp:LinkButton>
                    </ItemTemplate>
                    <SelectedItemStyle Font-Bold="True" />
                </asp:DataList>
            </SideBarTemplate>
            <StartNavigationTemplate>
                
            </StartNavigationTemplate>
            <WizardSteps>
                <asp:WizardStep ID="WizardStep1" runat="server" title="Upload an image" StepType="Auto">
                    <div id="Div1">
                        <b>Step1: Upload image</b><br />
                        Use the button below to upload a new image.<br />
                        <span style="color:Red">(Hint) The image must be 1600x1050.</span><br />
                        <asp:FileUpload ID="FileToUpload" runat="server" EnableTheming="False" style="width:300px;"/>
                        <br />
                        <asp:RequiredFieldValidator ID="valFileToUpload" runat="server" 
                            ErrorMessage="An image file name is required" ControlToValidate="FileToUpload" 
                            Display="Dynamic" ></asp:RequiredFieldValidator>
                         <br /><br />
                         <b>Step2: Choose a categoty</b><br />
                        Use dropdown list below to choose a category for the image.<br />
						<asp:DropDownList ID="CategoryDDL" runat="server">
						</asp:DropDownList><br />
                       <br />
                         <br />
                             <b>Step3: Add a caption</b><br />
                        <asp:TextBox ID="txtCaption" runat="server" style="width:250px;"></asp:TextBox>
                        <asp:Button ID="StartNextButton" runat="server" CommandName="MoveNext" 
                    Text="Next" onclick="StartNextButton_Click"  />
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ErrorMessage="A caption is required" ControlToValidate="txtCaption"></asp:RequiredFieldValidator>
                       
                        </div>
                    </asp:WizardStep>
                    
                <asp:WizardStep ID="WizardStep2" runat="server" title="Confirmation">
                    <b>Confirmation</b><br />
                    <br />
                    The image below was successfully uploaded.<br />
                    <br />
                    <asp:Image ID="iUploadedFile" runat="server" Width="500" Height="375" />
                    <br />
                    <b>Comment:</b>
                    <asp:Label ID="lCaptionConfirm" runat="server" Text="xxx"></asp:Label> <br />
                        
                </asp:WizardStep>
            </WizardSteps>
            <FinishNavigationTemplate>
                <asp:Button ID="FinishButton" runat="server" CommandName="MoveComplete" 
                    Text="Add another image" />
            </FinishNavigationTemplate>
            <StepNavigationTemplate>
                <asp:Button ID="StepNextButton" runat="server" CommandName="MoveNext" 
                    Text="Next" />
            </StepNavigationTemplate>
        </asp:Wizard>
        
        
        </div>
    <asp:XmlDataSource ID="xdsImages" runat="server" DataFile="~/Images.xml"></asp:XmlDataSource>
    <asp:HiddenField ID="hfFileName" runat="server" />
    <asp:HiddenField ID="hfThumbnailName" runat="server" />
    <asp:HiddenField ID="hfCaption" runat="server" />
 
    <asp:HiddenField ID="hfApplicationPath" runat="server" />
 
     <div id="menu">
        <br/><br/>
        <asp:HyperLink ID="hlAdminMenu" runat="server" 
            NavigateUrl="AdminMenu.aspx">Back to Admin Menu</asp:HyperLink>
    </div>
</asp:Content>