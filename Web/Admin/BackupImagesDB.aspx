﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/AdminMaster.master" AutoEventWireup="true" CodeFile="BackupImagesDB.aspx.cs" Inherits="Admin_BackupImagesDB" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" Runat="Server">
     
    
    

    <h2>Make a backup of the images database</h2>
    <br />
    <br />
    Click the buttons below to download a backup of the 4 sepereate database.<br />
     Keep them somwhere safe.<br />
<br />
<br />
     
    
         <asp:Button ID="bDownloadAdvertising" runat="server" Text="Download Advertising Database" 
         onclick="bDownloadAdvertising_Click" Width="250px" />
         
         <div id="menu">
      <br /><br />
    </div>
    <br />
     
    
         <asp:Button ID="bDownloadArchive" runat="server" Text="Download Archive Database" 
         onclick="bDownloadArchive_Click" Width="250px" />
         
         <br />
     
    
         <asp:Button ID="bDownloadEditorial" runat="server" Text="Download Editorial Database" 
         onclick="bDownloadEditorial_Click" Width="250px" />
         
         <br />
     
    
         <asp:Button ID="bDownloadStillLife" runat="server" Text="Download Still Life Database" 
         onclick="bDownloadStillLife_Click" Width="250px" />
         
         <br />

		  <asp:Button ID="bDownloadRecentWork" runat="server" Text="Download Recent Work Database" 
         onclick="bDownloadRecentWork_Click" Width="250px" />
         
         <br />
    <br />
        <asp:HyperLink ID="hlAdminMenu" runat="server" 
            NavigateUrl="AdminMenu.aspx">Back to Admin Menu</asp:HyperLink>
    </asp:Content>

