<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Gallery.aspx.cs" Inherits="Gallery" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <meta name="description" content="Damian Russell - Interiors and still life photographer. Represented by One Photographic. www.onephotographic.com." />
    <meta name="keywords" content="Damian Russell, London, photographer, Interiors, still, life, UK, art, photography, photo, onephotographic" />
    <meta name="author" content="http://www.damianrussell.com" />
    <meta name="copyright" content="Damian Russell&copy;2008" />
    <meta name="language" content="en" />
    <title>Damian Russell - Photographer - London - OnePhotographic - Still life - Interiors - Photography</title>
    <link rel="shortcut icon" href="../Content/Images/favicon.ico" type="image/x-icon" />
    <link href="../Styles/Shared.css" rel="stylesheet" type="text/css" media="all" />

    <script src="../Scripts/swfobject.js" type="text/javascript"></script>

    <script type="text/javascript">
        var flashvars = {};
        var params = { base: '', wmode: 'window', menu: 'false', scale: 'noscale', allowFullScreen: 'true' };
        var attributes = {};
        attributes.id = "index";
        swfobject.embedSWF('../index.swf', 'flashContainer', "100%", "100%", "9.0.0", false, flashvars, params, attributes);
    </script>

</head>
<body id="body" style="background-color: #000000; margin: 0px;">
    <div id="flashContainer">
    <script language="javascript" type="text/javascript">

        document.getElementById('body').setAttribute('style', 'background-color: #f5f0ea; margin: 0px');
    </script>
        <div id="nonflash">
            <div id="Logo">
                <img src="../Content/Images/NoFlashPage.jpg" alt="Damian Russull Photographer" title="Damian Russull Photographer"
                    border="0" usemap="#Map" />
                <map name="Map" id="Map">
              <area shape="poly" coords="591,456,591,456,728,529,756,483,620,406" href="http://www.onephotographic.com"
                        alt="OnePhotographic" title="OnePhotographic" target="_blank" />
                    <area shape="poly" coords="634,310,731,364,717,389,620,338" href="http://www.adobe.com/go/getflashplayer"
                        target="_blank" alt="Install Flash" title="Install Flash"/>
                </map>
            </div>
<div id="nonflash_Text">
                
                <h2>
                    Interiors and still life photographer</h2>
                <p><a href="http://www.onephotographic.com" target="_blank" style="text-decoration:none; color:#999999;" >
                    Represented by One Photographic<br />
                    48 Poland Street<br />
                    London<br />
                    W1F 7ND
                </p>
                <p>
                    Tel: +44 (0)207 287 2311<br />
                    Fax: +44 (0)207 287 2313
                </p>
            </div>
        </div>
    </div>
    <!--flashContainer end-->
    <%--Google analytics begin--%>


    <script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>

    <script type="text/javascript">
        var pageTracker = _gat._getTracker("UA-8346065-1"); pageTracker._trackPageview();
    </script>

    <%--Google analytics end--%>
</body>
</html>
