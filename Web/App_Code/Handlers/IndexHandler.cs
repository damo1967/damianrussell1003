﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// prevent unauthorised access to index.aspx during dev phase, amend once holding page no longer active.
/// </summary>
public class IndexHandler : IHttpHandler 
{
	
    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/xml";
        if (context.User.Identity.IsAuthenticated)
        {
            //context.Server.Transfer("~/Gallery/Gallery.aspx");
            context.Response.Redirect("Gallery/Gallery.aspx");
            //context.Response.Write();
        
        }
        else
        {
            context.Server.Transfer("~/Login.aspx");
        
        }
}

    public bool IsReusable
    {
        get
        {
            return true;
        }
    }
}
