﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.IO;

/// <summary>
/// Summary description for AppendImageXML
/// </summary>
public class AppendImageXML
{
        string sImage = "";
        string sCaption = "";
        string sPath = @"D:\DamienRussell\Oct 23 08\Web\";
    	private XmlDocument xmldoc;
	    

		public static void Main()
		{
			
		}

        public bool Append(string _sImage, string _sCaption)

		//Constructor

		{   sImage = _sImage;
            sCaption = _sCaption;
            

			FileStream fs = new FileStream(sPath,FileMode.Open,FileAccess.Read,FileShare.ReadWrite);
			xmldoc = new XmlDocument();
			xmldoc.Load(fs);
			AddImageXML();

            
            

            return true;
		}

		// Method for Displaying the AppendImageXML

		
		// Adding a new entry to the AppendImageXML

		private void AddImageXML()
		{


			// New XML Element Created
			XmlElement newAppendImageXMLentry = xmldoc.CreateElement("pic");

			// First Element - Book - Created
			XmlElement firstelement = xmldoc.CreateElement("image");

			// Value given for the first element
			firstelement.InnerText = sImage;

			// Append the newly created element as a child element
			newAppendImageXMLentry.AppendChild(firstelement);


			// Second Element - Publisher - Created
			XmlElement secondelement = xmldoc.CreateElement("caption");

			// Value given for the second element
			secondelement.InnerText = sCaption;

			// Append the newly created element as a child element
			newAppendImageXMLentry.AppendChild(secondelement);

			// New XML element inserted into the document
			xmldoc.DocumentElement.InsertBefore(newAppendImageXMLentry,xmldoc.DocumentElement.LastChild);

			// An instance of FileStream class created
			// First parameter is the sPath to our XML file - AppendImageXML.xml

			FileStream fsxml = new FileStream(sPath,FileMode.Truncate,FileAccess.Write,FileShare.ReadWrite);
			
			// XML Document Saved
			xmldoc.Save(fsxml);

          
		}



//end of class






}

